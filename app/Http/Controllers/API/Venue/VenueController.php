<?php

namespace App\Http\Controllers\Api\Venue;

use App\Repositories\VenueRepository;
use Illuminate\Http\Request;
use App\Http\Requests\VenueRequest;
use App\Http\Controllers\Api\ApiController;

class VenueController extends ApiController
{
    protected $venues;

    public function __construct(VenueRepository $venues)
    {
        parent::__construct();
        $this->venues = $venues;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->response->collection($this->venues->page());
    }

    /**
     * Show all of the categories.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList()
    {
        return $this->response->collection($this->venues->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\VenueRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VenueRequest $request)
    {
        $this->venues->store($request->all());
        return $this->response->withNoContent();
    }

    /**
     * Update Discussion Status By Discussion ID
     *
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function status($id, Request $request)
    {
        $input = $request->all();
        $this->venues->updateColumn($id, $input);
        return $this->response->withNoContent();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        return $this->response->item($this->venues->getById($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\VenueRequest $request
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(VenueRequest $request, $id)
    {
        $this->venues->update($id, $request->all());
        return $this->response->withNoContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->venues->destroy($id);
        return $this->response->withNoContent();
    }
}