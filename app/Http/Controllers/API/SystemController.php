<?php
/**
 * Created by PhpStorm.
 * User: usersss
 * Date: 17/11/2017
 * Time: 8:49 PM
 */

namespace App\Http\Controllers\Api;

use App\Models\Support;
use Illuminate\Http\Request;

class SystemController extends ApiController
{
    /**
     * SystemController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get the system info.
     *
     * @return mixed
     */
    public function getSystemInfo()
    {
        $pdo = \DB::connection()->getPdo();
        $version = $pdo->query('select version()')->fetchColumn();
        $data = [
            'server' => $_SERVER['SERVER_SOFTWARE'],
            'http_host' => $_SERVER['HTTP_HOST'],
            'remote_host' => isset($_SERVER['REMOTE_HOST']) ? $_SERVER['REMOTE_HOST'] : $_SERVER['REMOTE_ADDR'],
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
            'php' => phpversion(),
            'sapi_name' => php_sapi_name(),
            'extensions' => implode(", ", get_loaded_extensions()),
            'db_connection' => isset($_SERVER['DB_CONNECTION']) ? $_SERVER['DB_CONNECTION'] : 'Secret',
            'db_database' => isset($_SERVER['DB_DATABASE']) ? $_SERVER['DB_DATABASE'] : 'Secret',
            'db_version' => $version,
        ];
        return $this->response->json($data);
    }

    public function getSupportDetails()
    {
        $data = Support::all()->first();
        return $this->response->json($data);
    }

    public function updateSupportDetails(Request $request)
    {
        return $support = Support::updateOrCreate(['terms_and_agreement' => $request->get('termsAndAgreement'),
            'contact_details' => $request->get('contactDetails'), 'revision_no' => $request->get('revisionNo')]);

    }
}