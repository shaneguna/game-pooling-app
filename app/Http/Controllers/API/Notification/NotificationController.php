<?php

namespace App\Http\Controllers\Api\Notification;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;

class NotificationController extends ApiController
{
    protected $user_notifications;

    public function __construct()
    {
        parent::__construct();

        $this->user_notifications = auth()->guard('api')->user();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getListByActiveUser()
    {
        return $this->response->json($this->user_notifications->notifications,[201]);
    }

}