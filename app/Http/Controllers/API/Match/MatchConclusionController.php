<?php

namespace App\Http\Controllers\Api\Match;

use App\Repositories\MatchConclusionRepository;
use Illuminate\Http\Request;
use App\Models\MatchConclusion;
use App\Http\Controllers\Api\ApiController;

class MatchConclusionController extends ApiController
{
    protected $matches;

    public function __construct(MatchConclusionRepository $matches)
    {
        parent::__construct();
        $this->matches = $matches;
    }

    public function index(){
        return $this->response->colletion($this->matches->page());
    }

    public function store(Request $request){
        return $this->response->json($this->matches->store($request->all()));
    }

    public function update(Request $request,$id){
        $parameters = $request->all();

        MatchConclusion::where('id',$id)->update($parameters);

        return $this->response->withNoContent();
    }
}


