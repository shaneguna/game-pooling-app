<?php

namespace App\Http\Controllers\Api\Match;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\MatchMultipleFormRequest;
use App\Repositories\MatchRepository;
use App\Repositories\MatchRulesRepository;
use App\Repositories\MatchVenueDetailsRepository;
use App\Repositories\TeamRepository;
use Illuminate\Http\Request;
use App\Models\Match;


class MatchController extends ApiController
{
    protected $matches, $matchVenueDetails, $matchRules, $teams;

    public function __construct(MatchRepository $matches, MatchVenueDetailsRepository $matchVenueDetails, MatchRulesRepository $matchRules, TeamRepository $teams)
    {
        parent::__construct();
        $this->matches           = $matches;
        $this->matchRules        = $matchRules;
        $this->matchVenueDetails = $matchVenueDetails;
        $this->teams             = $teams;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->response->collection($this->matches->page());
    }

    /**
     * Show all of the matches.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        $arguments = $request->all();

        if(!isset($arguments['player_id']) && !isset($arguments['player_id'])){
            return $this->response->json(Match::with(['matchTeamCreator.teamLeader','matchPlayerCreator'])->get());
        }

        return $this->response->json([
            'data' => $this->matches->getListMatchesWithDistinction($arguments),
        ]);
    }

    public function matchesTeamInvitesOrRequestsByPlayerID(Request $request)
    {
        $arguments = $request->all();
        $game_id   = (isset($arguments['game_id']) && !empty($arguments['game_id'])) ?
        $arguments['game_id'] : 0;

        return $this->response->json([
            'data' => $this->matches->teamsMatchInvitesOrRequests($arguments['player_id'],
                $game_id),
        ]);
    }

    public function matchesPlayerInvitesOrRequestsByPlayerID(Request $request)
    {
        $arguments = $request->all();
        $game_id   = (isset($arguments['game_id']) && !empty($arguments['game_id'])) ?
        $arguments['game_id'] : 0;

        return $this->response->json([
            'data' => $this->matches->matchesPlayerInvitesOrRequests($arguments['player_id'],
                $game_id),
        ]);
    }

    public function byPage(Request $request)
    {
        return $this->response->collection($this->matches->getByPage($request->input('offset'), $request->input('limit')));
    }

    public function getByMatchId($id)
    {
        return $this->response->item($this->matches->getById($id));
    }

    public function getMatchRulesByMatchId($id)
    {
        return $this->response->json($this->matches->matchRulesByMatchId($id));
    }

    public function getMatchParticipantsByMatchId(Request $request)
    {
        $match_id = $request->get('match_id');
        $mode     = $request->get('match_mode');
        return $this->response->json($this->matches->matchParticipantsByMatchId($match_id, $mode));
    }

    /**
     * Show all of the matches by game id.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getListByGameId($id)
    {
        return $this->response->collection($this->matches->getAllByGameId($id));
    }

    /**
     * Show all globally visible matches.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGlobalMatches()
    {
        return $this->response->collection($this->matches->getAllGlobalMatches());
    }

    /**
     * Show all player matches by game.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMatchesByPlayerAndGame(Request $request)
    {
        $limit = $request->get('limit', 5);
        $game  = $request->get('game_id');
        $id    = $request->get('player_id');
        return $this->response->collection($this->matches->getByPlayerAndGameId($id, $limit, $game));
    }

    /**
     * Show all player matches.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMatchesByPlayerID(Request $request, $id)
    {
        if ($id > 0) {
            $limit = $request->get('limit', 5);

            return $this->response->collection($this->matches->getByPlayerId($id, $limit));
        }
        return $this->response->json([]);
    }

    /**
     * Show all player matches.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPlayersInMatches(Request $request, $match_id)
    {
        return $this->response->json($this->matches->matchParticipants($match_id));
    }

    /**
     * Link match to requesting player.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function joinMatch(Request $request)
    {
        return $this->response->collection($this->matches->joinMatch($request));
    }

    public function getMatchesByVicinity(Request $request)
    {
        return $this->response->collection($this->matches->matchesByVicinity($request));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\MatchMultipleFormRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(MatchMultipleFormRequest $request)
    {
        $arguments    = $request->all();
        $matchData    = $request['matches'];
        $participants = $arguments['match_participants'];

        if ($matchData['match_mode'] === 'Team Mode') {
            $matchData['team_creator']   = $participants['ally']['id'];
            $matchData['player_creator'] = 0;
        } else {
            $matchData['team_creator']   = 0;
            $matchData['player_creator'] = $arguments['player_id'];
        }

        $matchData['match_mode'] = ($matchData['match_mode'] === 'Team Mode') ? 'Team' : 'Solo';
        $match                   = $this->matches->store($matchData);
        $match_id                = $match->match_id;

        // Create relational entry to match_rules and match_venue tables
        $request->request->add(['matches.match_id', $match_id]);
        $this->matchRules->store(array_merge($request->input('match_rules'), ['match_id' => $match_id]));
        $this->matchVenueDetails->store(array_merge($request->input('match_venue'), ['match_id' => $match_id]));

        $match->matchPlayer()->attach($request->input('player_id'));

        if ($match->match_mode == 'Team') {

            if (!empty($participants['foe'])) {
                foreach ($participants['foe'] as $key => $value) {
                    \DB::table('match_invites')->insert([
                        'match_id'    => $match->match_id,
                        'team_id'     => $value['id'],
                        'type'        => 'Pending',
                        'invite_mode' => 'invited',
                    ]);
                }
            }

            if ($request->has('match_participants.ally.id')) {
                $match->matchTeams()->attach($match->team_creator);
            }

        } else if ($match->match_mode == 'Solo') {

            if (!empty($participants['foe'])) {
                foreach (!empty($participants['foe']) as $value) {
                    \DB::table('match_invites')->insert([
                        'match_id'    => $match->match_id,
                        'player_id'   => $value['player_id'],
                        'type'        => 'Pending',
                        'invite_mode' => 'invited',
                    ]);
                }
            }
        }
        return $this->response->withNoContent();
    }

    /**
     * Update Discussion Status By Discussion ID
     *
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function status($id, Request $request)
    {
        $input = $request->all();
        $this->matches->updateColumn($id, $input);
        return $this->response->withNoContent();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        return $this->response->item($this->matches->getById($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\MatchMultipleFormRequest $request
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        Match::where('match_id',$id)->update($request->all());
        return $this->response->withNoContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->matches->destroy($id);
        return $this->response->withNoContent();
    }

    public function getPossibleMatchPlayers(Request $request)
    {
        return $this->response->json($this->matches->formulatePossibleMatchPlayers($request->input('player_id'), $request->input('game_id'), $request->input('match_mode')));
    }

    public function requestToJoinMatch(Request $request)
    {
        return $this->response->json($this->matches->sendRequestToJoinMatch($request->all()));
    }

    public function getMatchDetails(Request $request)
    {
        return $this->response->json($this->matches->generateMatchDetails($request->all()));
    }

    public function updateMatchInviteStatus(Request $request)
    {
        return $this->response->json($this->matches->updateInviteStatus($request->all()));
    }

    public function updateMatchConfirmation(Request $request)
    {
        return $this->response->json($this->matches->updateMatchConfirmation($request->all()));
    }
}
