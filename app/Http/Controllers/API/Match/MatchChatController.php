<?php

namespace App\Http\Controllers\Api\Match;

use App\Repositories\MatchChatRepository;
use Illuminate\Http\Request;
use App\Http\Requests\MatchRequest;
use App\Http\Controllers\Api\ApiController;

class MatchChatController extends ApiController
{
    protected $matches;

    public function __construct(MatchChatRepository $matches)
    {
        parent::__construct();
        $this->matches = $matches;
    }


    public function sendMessage(Request $request)
    {
        return $this->matches->matchChatMessage($request->all());
    }

    public function matchMessages(Request $request)
    {
        return $this->response->json ($this->matches->getMatchMessages($request->all()));
    }

    public function isChatEnabled($match_id,Request $request){
        return $this->response->json ($this->matches->isChatEnabled($match_id,$request->all()));   
    }


}


