<?php

namespace App\Http\Controllers\Api\Game;

use Illuminate\Http\Request;
use App\Http\Requests\GameRequest;
use App\Repositories\GameRepository;
use App\Http\Controllers\Api\ApiController;

class GameController extends ApiController
{
    protected $games;

    /**
     * Class constructor
     *
     */
    public function __construct(GameRepository $games)
    {
        parent::__construct();
        $this->games = $games;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->response->collection($this->games->page());
    }

    /**
     * Show all of the categories.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList()
    {
        return $this->response->collection($this->games->all());
    }

    public function byPage(Request $request)
    {
        return $this->response->collection($this->games->getByPage($request->input('offset'),$request->input('limit')));
    }


    /**
     * Show all of the games matching the specific string by id or by game name.
     *
     * @param  $string
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSearchedResult($string)
    {
        return $this->response->collection($this->games->getSearchedResult($string));
    }


    public function getGameDetailsById($id)
    {
        return $this->response->json($this->games->getById($id));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\GameRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(GameRequest $request)
    {
        $this->games->store($request->all());
        return $this->response->withNoContent();
    }

    /**
     * Update Discussion Status By Discussion ID
     *
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function status($id, Request $request)
    {
        $input = $request->all();
        $this->games->updateColumn($id, $input);
        return $this->response->withNoContent();
    }

    /**
     * Get a collection of games based on the category ID
     *
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGamesByCategory($id)
    {
        return $this->response->collection($this->games->getByCategory($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        return $this->response->item($this->games->getById($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\GameRequest $request
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(GameRequest $request, $id)
    {
        $this->games->update($id, $request->all());
        return $this->response->withNoContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->games->destroy($id);
        return $this->response->withNoContent();
    }
}