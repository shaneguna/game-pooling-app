<?php

namespace App\Http\Controllers\Api\SocialNetwork;

use App\Events\FriendRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\SocialNetworkRepository;
use App\Notifications\UserAdded;
use App\Models\Game;
use App\Models\Player;
use App\Models\User;

class SocialNetworkController extends ApiController
{
    protected $social, $game;

    public function __construct(SocialNetworkRepository $social, Game $game)
    {
        parent::__construct();
        $this->social = $social;
        $this->game = $game;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendFriendRequest(Request $request)
    {
        $data = ['id' => '',
            'shown' => false,
            'inset' => true,
            'name' => $request->get('recipient_name'),
            'divider' => true,
            'title' => 'Hello',
            'message' => '',
            'profile_picture' => 'https://avatars0.githubusercontent.com/u/9064066?v=4&s=460'];

        $sender = auth()->guard('api')->user();

        $friend_request = $this->social->addFriend($request->get('recipient_id'));

        if (!$friend_request) {

            $data['name'] = $request->get('recipient_name');
            $data['title'] = 'Friend Request Received';
            $data['message'] = $request->get('recipient_name') . ", re-sent you friend a request. Would you like to check it?";

            $sender->notify(new UserAdded($data));

            event(new FriendRequest($data));

            return $this->response->withError('Sorry, a friend request already exists.');
        }

        $data['id'] = $sender->id;
        $data['name'] = 'Brian';
        $data['title'] = 'A title';
        $data['message'] = $request->get('recipient_name') . ", sent you a friend request.";

        $sender->notify(new UserAdded($data));

        event(new FriendRequest($data));

        return $this->response->json($friend_request, [201]);
    }


    public function getFriendList($id)
    {
        return $this->response->json($this->social->getFriends($id));
    }

    public function getUserPendingRequests($id)
    {
        return $this->response->json($this->social->getPendingRequests($id));
    }

    public function acceptPendingFriendRequest(Request $request)
    {

        $data = ['id' => '',
            'shown' => false,
            'inset' => true,
            'name' => $request->get('recipient_name'),
            'divider' => true,
            'title' => 'Hello',
            'message' => '',
            'profile_picture' => 'https://avatars0.githubusercontent.com/u/9064066?v=4&s=460'];

        $sender = auth()->guard('api')->user();

        $friend_request = $this->social->acceptFriendRequest($request->get('requester_id'));


        if (!$friend_request) {

            /* $data['name'] = $request->get('recipient_name');
             $data['title'] = 'Friend Request Received';
             $data['message'] = $request->get('recipient_name') . ", re-sent you friend a request. Would you like to check it?";

             $sender->notify(new UserAdded($data));

             event(new FriendRequest($data));

             return $this->response->withError('Sorry, a friend request already exists.');*/
        }

        /* $data['id'] = $sender->id;
         $data['name'] = 'Brian';
         $data['title'] = 'A title';
         $data['message'] = $request->get('recipient_name') . ", sent you a friend request.";

         $sender->notify(new UserAdded($data));

         event(new FriendRequest($data));*/

        return $this->response->json($friend_request, [201]);
    }

    public function getFavoriteGames(Request $request)
    {
        if($request->get('player_id') > 0){
            $player = Player::find($request->get('player_id'));
            $user = User::find($player->user_id);
            return $this->response->json($user->favorites($this->game)->get(), [201]);            
        }

        return $this->response->json([], [201]);            
    }

    public function addFavoriteGame(Request $request)
    {
        $response = [];
        foreach($request->get('game_id') as $game) {
            $player = $request->get('player_id');
            $user = User::find(Player::find($player)->user_id);

            $response[$game] = !$user->hasFavorited(Game::find($game)) ? $this->response->json($user->toggleFavorite(Game::find($game))) : $this->response->withError('Sorry, you already added this game as a favorite.');
        }

        return $response;
    }

    public function removeFavoriteGame(Request $request)
    {
        $game = $request->get('game_id');
        $player = $request->get('player_id');
        $user = User::find(Player::find($player)->user_id);

        return $user->hasFavorited(Game::find($game)) ? $user->unfavorite(Game::find($game)) : $this->response->withError('Sorry, did not find that game under your favorites.');
    }
}