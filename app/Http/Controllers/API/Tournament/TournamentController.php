<?php

namespace App\Http\Controllers\API\Tournament;

use App\Http\Controllers\Api\ApiController;
use App\Repositories\TournamentRepository;
use Illuminate\Http\Request;

class TournamentController extends ApiController
{
    protected $tournament;

    public function __construct(TournamentRepository $tournament)
    {
        parent::__construct();
        $this->tournament = $tournament;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->response->collection($this->tournament->page(10, 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->tournament->store($request->all());
        return $this->response->withNoContent();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return json_encode($this->tournament->getTournamentDetails($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return json_encode($this->tournament->getTournamentDetails($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->tournament->update($id, $request->all());
        return $this->response->withNoContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::table('tournament_applicants')->where('tournament_id', $id)->delete();
        \DB::table('tournament_contestants')->where('tournament_id', $id)->delete();
        $this->tournament->destroy($id);
        $this->response->withNoContent();
    }

    public function updateTournamentContestant($id, Request $request)
    {
        return json_encode($this->tournament->updateTournamentContestant($id, $request->all()));
    }

    public function getListOfContestant($id)
    {
        return $this->response->json($this->tournament->generateContestantsByTournamentID($id));
    }

    public function addContestant(Request $request)
    {
        $data = $request->all();
        return $this->response->json($this->tournament->insertTournamentApplicant($data));
    }

    public function updateImage($id, Request $request)
    {
        $data                  = $request->all();
        $data['tournament_id'] = $id;
        $this->tournament->updateTournamentImage($data);
        return $this->response->withNoContent();
    }
}
