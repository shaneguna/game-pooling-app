<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use App\Repositories\SocialiteRepository;
use App\Repositories\LoginProxyRepository as LoginProxy;
use App\Models\User;
use Socialite;

class SocialiteLoginController extends Controller
{

    private $loginProxy;

    public function __construct(LoginProxy $loginProxy)
    {
        $this->loginProxy = $loginProxy;
    }


	/**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->stateless()->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback(SocialiteRepository $socialRepo)
    {
	    $user =	Socialite::driver('facebook')->stateless()->user();

        $socialRepo->createOrGetUser($user->user);

        return redirect('/');    				
    }


    public function loginFacebook(Request $request,SocialiteRepository $socialRepo){
            $access_token = $request->get('access_token');
            $user = Socialite::driver('facebook')->userFromToken($access_token);
            $socialRepo->createOrGetUser($user->user);
    
            return Response::json($this->loginProxy->facebookLogin($user->token, $user->email));
    }
    
     
}
