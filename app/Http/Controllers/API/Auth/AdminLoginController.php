<?php

namespace App\Http\Controllers\API\Auth;
use App\Repositories\LoginProxyRepository as LoginProxy;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use Response;
use App\Http\Controllers\Controller;

class AdminLoginController extends Controller
{

    private $loginProxy;

    public function __construct(LoginProxy $loginProxy)
    {
        $this->loginProxy = $loginProxy;
    }

    public function login(Request $request)
    {
        $email = $request->get('username');
        $password = $request->get('password');
        $type = $request->get('type');
        
        return Response::json($this->loginProxy->attemptLogin($email, $password,$type));
    }

    public function refresh(Request $request)
    {
        return Response::json($this->loginProxy->attemptRefresh());
    }

    public function logout(Request $request)
    {
        $this->loginProxy->logout($request);
        return Response::json("Access Revoke");
    }
}