<?php

namespace App\Http\Controllers\Api\Team;

use App\Repositories\TeamRepository;
use App\Models\TeamInfo;
use Illuminate\Http\Request;
use App\Http\Requests\TeamRequest;
use App\Http\Controllers\Api\ApiController;

class TeamController extends ApiController
{
    protected $teams;

    public function __construct(TeamRepository $teams)
    {
        parent::__construct();
        $this->teams = $teams;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->response->collection($this->teams->page());
    }

    /**
     * Show all of the categories.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request)
    {
        return $this->response->json($this->teams->getTeamList($request->get('show_inactive',true)));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TeamRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TeamRequest $request)
    {
        return $this->response->item($this->teams->createTeamDefault($request->all()));
    }

    /**
     * Update Discussion Status By Discussion ID
     *
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function status($id, Request $request)
    {
        $input = $request->all();
        $this->teams->updateColumn($id, $input);
        return $this->response->withNoContent();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        return $this->response->json(TeamInfo::with(['category','game'])->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\TeamRequest $request
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(TeamRequest $request, $id)
    {
        $this->teams->update($id, $request->all());
        return $this->response->withNoContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $model = TeamInfo::find($id);
        $model->status = 0;
        $model->save();
        return $this->response->withNoContent();
    }

    /**
     * Display a listing of the teams by player id.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTeamsByPlayer(Request $request)
    {
        return $this->response->json($this->teams->teamsByPlayer($request->player_id));
    }

    /**
     * Matches of the team.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateTeamMatches($id)
    {
        return $this->response->json($this->teams->getTeamMatches($id));
    }

    public function getTeamMembers($id){
        return $this->response->json($this->teams->getTeamMembers($id));
    }


    public function leaveTeam(Request $request)
    {
        return $this->response->withNoContent($this->teams->leaveTeamByPlayerID($request->all()));
    }
}