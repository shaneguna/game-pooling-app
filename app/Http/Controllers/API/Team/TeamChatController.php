<?php

namespace App\Http\Controllers\API\Team;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\TeamChatRepository;


class TeamChatController extends ApiController
{
    protected $chat;

    public function __construct(TeamChatRepository $chat)
    {
        parent::__construct();
        $this->chat = $chat;
    }

    public function sendMessage(Request $request)
    {
        return $this->chat->teamChatMessage($request->all());
    }

    public function teamMessages(Request $request)
    {
        return $this->response->json ($this->chat->getTeamMessages($request->all()));
    }

    public function unreadTeamMessages(Request $request){
        return $this->response->json ($this->chat->getUnreadTeamChat($request->all()));
    }
    public function updateTeamNotifications(Request $request){
        return $this->response->json ($this->chat->updateTeamNotif($request->all()));
    }

}
