<?php

namespace App\Http\Controllers\API\Team;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\TeamRepository;

class TeamInviteController extends ApiController
{

    protected $team;

    public function __construct(TeamRepository $team)
    {
        parent::__construct();
        $this->team = $team;
    }

    public function invitationList($team_id)
    {
        return $this->response->json($this->team->teamListInvitation($team_id));
    }

    public function teamInvitation(Request $request)
    {
      $result = $this->team->invitePlayer($request->get('nickname'),$request->get('team_id'));
      return $result;
    }

    public function joinTeam(Request $request)
    {
	  $this->team->joinTeamRequest($request->get('player_id'),$request->get('team_id'));
      return $this->response->withNoContent();	
    }

    public function acceptInvite(Request $request)
    {
  	  $this->team->acceptInvitation($request->get('player_id'),$request->get('team_id'));
	  return $this->response->withNoContent();
    }

    public function rejectInvite(Request $request)
    {
  	  $this->team->rejectInvitation($request->get('player_id'),$request->get('team_id'));
	  return $this->response->withNoContent();
    }
}
