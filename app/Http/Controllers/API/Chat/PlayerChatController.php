<?php

namespace App\Http\Controllers\API\Chat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\PlayerChatRepository;
use App\Http\Controllers\Api\ApiController;


class PlayerChatController extends ApiController
{
	protected $chat;

    public function __construct(PlayerChatRepository $chat)
    {
        parent::__construct();
        $this->chat = $chat;
    }

    public function message($request)
    {
    	$this->chat->store($request->all());
    }
}
