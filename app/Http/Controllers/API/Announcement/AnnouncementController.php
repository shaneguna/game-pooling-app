<?php

namespace App\Http\Controllers\Api\Announcement;

use Illuminate\Http\Request;
use App\Http\Requests\AnnouncementRequest;
use App\Repositories\AnnouncementRepository;
use App\Http\Controllers\Api\ApiController;

class AnnouncementController extends ApiController
{
    protected $announcements;

    public function __construct(AnnouncementRepository $announcement)
    {
        parent::__construct();
        $this->announcements = $announcement;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->response->collection($this->announcements->page());
    }

    /**
     * Show all of the announcements.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList()
    {
        return $this->response->collection($this->announcements->page());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AnnouncementRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AnnouncementRequest $request)
    {
        $this->announcements->store($request->all());
        return $this->response->withNoContent();
    }

    /**
     * Update Discussion Status By Discussion ID
     *
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function status($id, Request $request)
    {
        $input = $request->all();
        $this->announcements->updateColumn($id, $input);
        return $this->response->withNoContent();
    }

    /**
     * Get a collection of games based on the category ID
     *
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAnnouncementsById($slug)
    {
        $slug = explode('-', $slug);
        $slug = array_slice($slug, 0);
        return $this->response->item($this->announcements->getById($slug[0]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        return $this->response->item($this->announcements->getById($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\AnnouncementRequest $request
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(AnnouncementRequest $request, $id)
    {
        $this->announcements->update($id, $request->all());
        return $this->response->withNoContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->announcements->destroy($id);
        return $this->response->withNoContent();
    }
}