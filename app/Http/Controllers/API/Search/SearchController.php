<?php

namespace App\Http\Controllers\Api\Search;

use Illuminate\Http\Request;
use App\Repositories\SearchRepository;
use App\Http\Controllers\Api\ApiController;

class SearchController extends ApiController
{
    protected $search;

    public function __construct(SearchRepository $search)
    {
        parent::__construct();
        $this->search = $search;
    }

    /**
     * Show all of the games matching the specific string by id or by game name.
     *
     * @param  $string
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSearchedResult($string)
    {

        $result = [
            "players" => $this->search->searchByPlayer($string),
            "games" => $this->search->searchByGame($string),
            "categories" => $this->search->searchByCategory($string),
            "teams" => $this->search->searchByTeam($string)
        ];

        return $this->response->json($result,[200]);
    }
}