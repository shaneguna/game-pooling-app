<?php

namespace App\Http\Controllers\Api\Player;

use App\Repositories\PlayerRepository;
use Illuminate\Http\Request;
use App\Http\Requests\PlayerRequest;
use App\Http\Controllers\Api\ApiController;

class PlayerController extends ApiController
{
    protected $players;



    public function __construct(PlayerRepository $players)
    {
        parent::__construct();
        $this->players = $players;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->response->collection($this->players->page());
    }

    /**
     * Show all of the categories.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList()
    {
        return $this->response->collection($this->players->all());
    }

    /**
     * Show all of the categories.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPlayerByID($id)
    {
       return $this->response->item($this->players->getByPlayerID($id));
    }


    public function byPage(Request $request)
    {
        return $this->response->collection($this->players->getByPage($request->input('offset'),$request->input('limit')));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PlayerRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->response->json($this->players->createPlayer($request));
    }

    /**
     * Update Discussion Status By Discussion ID
     *
     * @param $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function status($id, Request $request)
    {
        $input = $request->all();
        $this->players->updateColumn($id, $input);
        return $this->response->withNoContent();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        return $this->response->item($this->players->getById($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PlayerRequest $request
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PlayerRequest $request, $id)
    {
        $this->players->update($id, $request->all());
        return $this->response->withNoContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->players->destroy($id);
        return $this->response->withNoContent();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function teamMembers(Request $request)
    {
        return $this->response->collection($this->players->playersByTeamID($request->team_id));
    }

    public function getPlayerGameFavorites(Request $request)
    {
        return $this->response->collection($this->players->getFavoriteGames($request->input('player_id')));
    }

    /**
     * Display a listing of the teams own by this player.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTeamsLeadByPlayer($player_id)
    {
        return $this->response->json($this->players->generateTeamsLeadByPlayer($player_id));
    }
    

}