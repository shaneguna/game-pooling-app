<?php

namespace App\Http\Controllers\API\Player;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Repositories\PlayerChatRepository;


class PlayerChatController extends ApiController
{
    protected $chat;

    public function __construct(PlayerChatRepository $chat)
    {
        parent::__construct();
        $this->chat = $chat;
    }

    public function sendMessage(Request $request)
    {
        return $this->chat->sendMessage($request->all());
    }

    public function getPlayerMessageSender(Request $request)
    {
        return $this->response->json ($this->chat->generatePlayerMessageSender($request->all()));   
    } 

    public function getMessages(Request $request)
    {
        return $this->response->json ($this->chat->getPlayerMessages($request->all()));
    }
}
