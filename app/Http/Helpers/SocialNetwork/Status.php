<?php
namespace App\Http\Helpers\SocialNetwork;
/**
 * Social Network Friend Status.
 */
class Status
{
    const PENDING = 0;
    const ACCEPTED = 1;
    const DENIED = 2;
    const BLOCKED = 3;
}