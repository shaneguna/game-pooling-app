<?php

namespace App\Http\Helpers\Utils;

use Adaojunior\Passport\SocialGrantException;
use Adaojunior\Passport\SocialUserResolverInterface;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;


class SocialUserResolver implements SocialUserResolverInterface
{

  /**
   * Resolves user by given network and access token.
   *
   * @param string $network
   * @param string $accessToken
   * @param null $accessTokenSecret
   * @return \Illuminate\Http\JsonResponse|mixed
   * @throws SocialGrantException
   */
  public function resolve($network, $accessToken, $accessTokenSecret = null)
  {
      switch ($network) {
          case 'facebook':
              return $this->authWithFacebook($accessToken);
              break;
          default:
              throw SocialGrantException::invalidNetwork();
              break;
      }
  }


  /**
   * Resolves user by facebook access token.
   *
   * @param string $accessToken
   * @return \Illuminate\Http\JsonResponse
   */
  protected function authWithFacebook($accessToken)
  {
      $user = Socialite::driver('facebook')->userFromToken($accessToken);
      $id = \DB::table('users')->where('email',$user->email)->get()[0]->id;  
      $user  = User::find($id);

      if ( ! is_null($user) ) {
          return $user;
      }
      else {
          return response()->json([
              "message" => "invalid_credentials",
              "errors" => "The facebook's id of the user does not exist in the system.",
          ], 404);
      }
  }

}
