<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MatchDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'matches.match_name' => 'required|min:2|unique:matches',
            'matches.match_description' => 'required',
            'matches.match_mode' => 'required',
            'matches.match_date' => 'required',
            'matches.is_public' => 'required'
        ];
    }
}
