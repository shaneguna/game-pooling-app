<?php

namespace App\Http\Requests;

use App\MatchRules;
use Illuminate\Foundation\Http\FormRequest;

class MatchMultipleFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $form_requests = [
            MatchDetailsRequest::class,
            //MatchParticipantsRulesRequest::class,
            MatchRulesRequest::class
        ];

        $rules = [];

        foreach ($form_requests as $source) {
            $rules = array_merge(
                $rules,
                (new $source)->rules()
            );
        }

        return $rules;
    }
}
