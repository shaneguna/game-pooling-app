<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MatchConfirmation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'match_id',
        'team_creator_id',
        'player_creator_id',
        'team_opponent_id',
        'player_opponent_id',
        'creator_status',
        'opponent_status',
    ];

    public function opponent_team_info()
    {
        return $this->belongsTo('App\Models\TeamInfo', 'team_opponent_id', 'team_id');
    }

    public function opponent_player()
    {
        return $this->belongsTo('App\Models\Player', 'player_opponent_id', 'player_id');
    }

}
