<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Overtrue\LaravelFollow\Traits\CanBeFavorited;

class Game extends Model
{
    use CanBeFavorited;

    protected $primaryKey = 'game_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'game_name', 'category_id', 'game_description', 'background_image', 'image', 'team_member_minimum'
    ];

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function matches()
    {
        return $this->hasMany('App\Models\Match', 'game_id');
    }

}
