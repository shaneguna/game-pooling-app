<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MatchInvite extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'match_id', 'team_id', 'player_id', 'invite_mode', 'type',
    ];

    public function match()
    {
        return $this->belongsTo('App\Models\Match');
    }

    public function teamInfo()
    {
        return $this->belongsTo('App\Models\TeamInfo','team_id');
    }


}
