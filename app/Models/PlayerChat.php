<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;


class PlayerChat extends Model
{

    use Notifiable;

    protected $primaryKey = "player_chat_id";
    public $table = "player_chat";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'player_receiver', 'player_sender', 'message','created_at', 'updated_at','channel'
    ];

    /**
     * A message belong to a player
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function receiver()
    {
      return $this->belongsTo('App\Models\User','player_id','player_receiver');
    }


    /**
     * A sender player
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
      return $this->belongsTo('App\Models\User','player_id','player_sender');
    }

}
