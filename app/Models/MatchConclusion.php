<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MatchConclusion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'match_id', 'admin_user_id', 'admin_notes', 'creator_claim', 'creator_statement', 'creator_image_attachment',
        'creator_document_attachment', 'opponent_claim', 'opponent_statement', 'opponent_image_attachment', 'opponent_document_attachment', 'judgement_to_creator', 'judgement_to_opponent',
    ];

    public function match()
    {
        return $this->belongsTo('App\Models\Match','match_id');
    }


}
