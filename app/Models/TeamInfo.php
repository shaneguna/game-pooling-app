<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamInfo extends Model
{
    protected $primaryKey = 'team_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'team_name', 'team_leader', 'team_status', 'team_win', 'team_loss', 'team_rating', 'team_description', 'category_id', 'game_id', 'status', 'accept_request', 'is_temporary','image','background_image'
    ];

    public function matches()
    {
        return $this->belongsToMany('App\Models\Match', 'match_team',
            'team_id', 'match_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category','category_id','category_id');
    }

    public function game()
    {
        return $this->belongsTo('App\Models\Game','game_id','game_id');
    }

    public function teamLeader()
    {
        return $this->belongsTo('App\Models\Player','team_leader','player_id');
    }


    public function members()
    {
        return $this->belongsToMany('App\Models\Player', 'team_member',
            'team_id', 'player_id');
    }

    /**
     * Scope for non-temporary teams
     *
     * @param $query
     * @return mixed
     */
    public function scopeNotTemporary($query)
    {
        return $query->where('is_temporary', 0);
    }


    public function team_leader_info()
    {
         return $this->belongsTo('App\Models\Player','team_leader','player_id');
    }
}
