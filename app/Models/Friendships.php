<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Friendships extends Model
{
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'background_image', 'description'
    ];
}
