<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Player extends Model
{
    use Notifiable;

    protected $primaryKey = 'player_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'player_nickname', 'player_rating', 'player_win', 'player_loss', 'player_details', 'public_profile'
    ];

    public function matches()
    {
        return $this->belongsToMany(
            'App\Models\Match',
            'match_player',
            'player_id',
            'match_id'
        );
    }

    public function matchesByGame($game)
    {
        return $this->belongsToMany(
            'App\Models\Match',
            'match_player',
            'player_id',
            'match_id'
        )->where('game_id', $game);
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function teams()
    {
        return $this->belongsToMany(
            'App\Models\TeamInfo',
            'team_member',
            'player_id',
            'team_id'
        );
    }

    public function playerGameFavorites()
    {
        return $this->belongsToMany(
            'App\Models\Game',
            'player_game_favorites',
            'player_id',
            'game_id'
        );
    }

    public function messages()
    {
        return $this->belongsToMany('App\Models\PlayerMessage', 'player_sender');
    }

    public function teams_owned()
    {
        return $this->hasMany('App\Models\TeamInfo', 'team_leader');
    }

    /**
     * Scope for public profiles
     *
     * @param $query
     * @return mixed
     */
    public function scopePublicProfile($query)
    {
        return $query->where('public_profile', 1);
    }
}
