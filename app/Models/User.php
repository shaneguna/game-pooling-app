<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hootlex\Friendships\Traits\Friendable;
use Overtrue\LaravelFollow\Traits\CanFavorite;

class User extends Authenticatable
{

    use Notifiable, HasApiTokens, Friendable, CanFavorite;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'first_name', 'last_name', 'password', 'role', 'gender', 'login_type', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function player()
    {
        return $this->hasOne('App\Models\Player','user_id');
    }

    /**
     * Get all of the teams for this user/player.
     */
    public function teams()
    {

           // 'App\Post',
           //  'App\User',
           //  'country_id', // Foreign key on users table...
           //  'user_id', // Foreign key on posts table...
           //  'id', // Local key on countries table...
           //  'id' // Local key on users table...
        return $this->hasManyThrough('App\Models\TeamInfo','App\Models\Player','user_id','team_leader','id','user_id');
    }
}
