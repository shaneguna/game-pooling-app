<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $primaryKey = 'match_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'match_name', 'match_description', 'match_mode', 'match_winner', 'match_score', 'match_status', 'match_date', 'game_id', 'is_public', 'created_by','team_creator','player_creator'
    ];

    /**
     * Player-Matches relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function player()
    {
        return $this->belongsToMany('App\Models\Player', 'match_player',
            'match_id', 'player_id');
    }

    public function teams()
    {
        return $this->belongsToMany('App\Models\TeamInfo');
    }

 
    public function matchInvites()
    {
        return $this->hasMany('App\Models\MatchInvite', 'match_id');
    }

    public function matchRules()
    {
        return $this->hasOne('App\Models\MatchRules', 'match_id');
    }

    public function matchConclusion()
    {
        return $this->hasOne('App\Models\MatchConclusion', 'match_id');
    }


    public function matchTeamCreator()
    {
        return $this->belongsTo('App\Models\TeamInfo', 'team_creator', 'team_id');
    }

    public function matchPlayerCreator()
    {
        return $this->belongsTo('App\Models\Player', 'player_creator', 'player_id');
    }


    public function matchConfirmation()
    {
        return $this->hasOne('App\Models\MatchConfirmation', 'match_id');
    }

    public function matchVenueDetails()
    {
        return $this->hasOne('App\Models\MatchVenueDetails', 'match_id');
    }

    public function gameDetails()
    {
        return $this->belongsTo('App\Models\Game', 'game_id', 'game_id');
    }

    /*public function matchParticipants()
    {
    return $this->belongsToMany('App\Models\TeamInfo', 'match_participants', 'match_id', 'team_id');
    }*/

    public function matchPlayer()
    {
        return $this->belongsToMany('App\Models\Player', 'match_player', 'match_id', 'player_id');
    }

    public function matchTeams()
    {
        return $this->belongsToMany('App\Models\TeamInfo', 'match_team', 'match_id', 'team_id');
    }

    /**
     * Scope for Globally visible matches
     *
     * @param $query
     * @return mixed
     */
    public function scopeGlobalMatches($query)
    {
        return $query->where('is_public', 1);
    }

}
