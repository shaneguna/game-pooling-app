<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TournamentApplicant extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tournament_id', 'player_id', 'team_id'
    ];

    public function tournament()
    {
        return $this->belongsTo('App\Models\Tournament', 'tournament_id');
    }

    public function player()
    {
        return $this->belongsTo('App\Models\Player', 'player_id');
    }

    public function team()
    {
        return $this->belongsTo('App\Models\TeamInfo', 'team_id');
    }




}
