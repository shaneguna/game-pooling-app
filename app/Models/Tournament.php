<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    protected $primaryKey = 'tournament_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tournament_name', 'tournament_description','image','category_id','game_id','start_date','type','contestant_number'
    ];

    public function tournament_applicant()
    {
        return $this->hasMany('App\Models\TournamentApplicant','tournament_id');
    }

    public function tournament_contestant()
    {
        return $this->hasMany('App\Models\TournamentContestant','tournament_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function game()
    {
        return $this->belongsTo('App\Models\Game', 'game_id');
    }


    public function scopeContestants($query){
        return $query->when($this->type === 'Team',function($query){
            return $query->with(['tournament_applicant.team','tournament_contestant.team']);  
        })->when($this->type === 'Individual',function($query){
            return $query->with(['tournament_applicant.player','tournament_contestant.player']);
        })->get();
    }

}

