<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamInvite extends Model
{
    protected $fillable = [
        'team_id', 'player_id', 'status', 'mode'
    ];

    public function player() 
    {
    	return $this->belongsTo('App\Models\Player','player_id','player_id');
    }
}
