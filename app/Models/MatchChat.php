<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class MatchChat extends Model
{

    use Notifiable;

    protected $table = "match_chat";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'match_id', 'player_id', 'message','created_at', 'updated_at'
    ];

    /**
     * A message belong to a player
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function match()
    {
      return $this->belongsTo('App\Models\Match','match_id','match_id');
    }
    public function player()
    {
      return $this->belongsTo('App\Models\Player','player_id','player_id');
    }


}