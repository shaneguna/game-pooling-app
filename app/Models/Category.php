<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $primaryKey = 'category_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_name',
        'category_description',
        'category_image',
        'background_image'
    ];

     public function games()
     {
           return $this->hasMany('App\Models\Game', 'category_id');
     }

}
