<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
    protected $primaryKey = 'venue_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'venue_name',
        'venue_description',
        'venue_image',
        'long',
        'lat',
        'capacity',
        'city',
        'address'
    ];

}
