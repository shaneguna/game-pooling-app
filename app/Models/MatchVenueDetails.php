<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MatchVenueDetails extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'match_id', 'venue_name', 'long', 'lat', 'vicinity'
    ];

}
