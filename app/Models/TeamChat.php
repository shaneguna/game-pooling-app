<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class TeamChat extends Model
{

    use Notifiable;

    protected $table = "team_chat";
    protected $primaryKey = "team_chat_id"; 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'team_id', 'player_id', 'message','created_at', 'updated_at'
    ];

    /**
     * A message belong to a player
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function team()
    {
      return $this->belongsTo('App\Models\TeamInfo','team_id','team_id');
    }
    public function player()
    {
      return $this->belongsTo('App\Models\Player','player_id','player_id');
    }

}
