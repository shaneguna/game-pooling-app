<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MatchRules extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'match_id', 'min_age', 'max_age', 'max_age', 'gender_rule'
    ];

    public function match()
    {
        return $this->belongsTo('App\Models\Match');
    }
}
