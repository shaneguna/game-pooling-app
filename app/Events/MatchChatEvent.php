<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\MatchChat as Chat;

class MatchChatEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $matchChat;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Chat $matchChat)
    {
        $this->matchChat = $matchChat;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('match-chat-'.$this->matchChat->match_id);
    }

    public function broadcastWith()
    {
        return [
            'message' => $this->matchChat->message,
            'player_nickname' => $this->matchChat->player->player_nickname
        ];
    }
}
