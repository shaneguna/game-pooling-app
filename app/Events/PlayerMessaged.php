<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\PlayerChat;
use App\Models\Player;

class PlayerMessaged implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $playerChat;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(PlayerChat $playerChat)
    {
        $this->playerChat = $playerChat;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('player-chat-'.$this->playerChat->channel);
    }


    public function broadcastWith()
    {
        return [        
                'sender' => Player::find($this->playerChat->player_sender)->player_nickname,
                'sender_id' => $this->playerChat->player_sender,
                'receiver_id' => $this->playerChat->player_receiver,
                'receiver' => Player::find($this->playerChat->player_receiver)->player_nickname,
                'seen' => $this->playerChat->seen,
                'message' => $this->playerChat->message, 
        ];
    }
}
