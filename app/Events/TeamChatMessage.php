<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\TeamChat;

class TeamChatMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $teamChat;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(TeamChat $teamChat)
    {
        $this->teamChat = $teamChat;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('team-chat-'.$this->teamChat->team_id);
    }

    public function broadcastWith()
    {
        return [
            'message' => $this->teamChat->message,
            'player_nickname' => $this->teamChat->player->player_nickname
        ];
    }
}
