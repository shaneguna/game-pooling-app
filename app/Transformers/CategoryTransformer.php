<?php

namespace App\Transformers;

use App\Models\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
    public function transform(Category $category)
    {
        return [
            'id' => $category->category_id,
            'category_name' => $category->category_name,
            'category_description' => $category->category_description,
            'category_image' => $category->category_image,
            'background_image' => $category->background_image,
            'created_at' => $category->created_at->toDateTimeString(),
            'games' => $category->games
        ];
    }
}