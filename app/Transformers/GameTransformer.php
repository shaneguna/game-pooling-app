<?php

namespace App\Transformers;

use App\Models\Game;
use League\Fractal\TransformerAbstract;

class GameTransformer extends TransformerAbstract
{
    public function transform(Game $game)
    {
        return [
            'id' => $game->game_id,
            'game_name' => $game->game_name,
            'game_description' => $game->game_description,
            'image' => $game->image,
            'background_image' => $game->background_image,
            'category_id' => $game->category_id,
            'created_at' => $game->created_at->toDateTimeString(),
            'category_name' => $game->category->category_name,
            'team_member_minimum' => $game->team_member_minimum,
            'matches' => $game->matches
        ];
    }
}