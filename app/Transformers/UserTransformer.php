<?php

namespace App\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
			'gender' => $user->gender,          
			'role' => $user->role,  
            'created_at' => ($user->created_at !== null) ? $user->created_at->toDateTimeString():"N/A",
            'updated_at' => ($user->updated_at !== null) ? $user->updated_at->toDateTimeString():"N/A",
        ];
 
    }
}