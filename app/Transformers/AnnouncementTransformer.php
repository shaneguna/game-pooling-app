<?php

namespace App\Transformers;

use App\Models\Announcement;
use League\Fractal\TransformerAbstract;

class AnnouncementTransformer extends TransformerAbstract
{
    public function transform(Announcement $announcement)
    {
        return [
            'id' => $announcement->id,
            'title' => $announcement->title,
             'link' => $announcement->link,
            'background_image' => $announcement->background_image,
            'description' => $announcement->description,
            'created_at' => $announcement->created_at->diffForHumans(),
        ];
    }
}