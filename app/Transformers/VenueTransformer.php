<?php

namespace App\Transformers;

use App\Models\Venue;
use League\Fractal\TransformerAbstract;

class VenueTransformer extends TransformerAbstract
{
    public function transform(Venue $venue)
    {
        return [
            'id'                => $venue->venue_id,
            'venue_name'        => $venue->venue_name,
            'venue_description' => $venue->venue_description,
            'venue_image'       => $venue->venue_image,
            'vicinity'          => $venue->city,
            'long'              => $venue->long,
            'lang'              => $venue->lang,
            'created_at'        => $venue->created_at->toDateTimeString(),
        ];
    }
}
