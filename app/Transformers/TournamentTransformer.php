<?php

namespace App\Transformers;

use App\Models\tournament;
use League\Fractal\TransformerAbstract;

class TournamentTransformer extends TransformerAbstract
{
    public function transform(tournament $tournament)
    {
        return [
            'tournament_id' => $tournament->tournament_id,
            'tournament_name' => $tournament->tournament_name,
            'type' => $tournament->type,
            'contestant_number' => $tournament->contestant_number,
            'team_description' => $tournament->team_description,
            'start_date' => $tournament->start_date,
            'created_at' => $tournament->created_at->toDateTimeString(),
        ];
    }
}