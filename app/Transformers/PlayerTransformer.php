<?php

namespace App\Transformers;

use App\Models\Player;
use League\Fractal\TransformerAbstract;

class PlayerTransformer extends TransformerAbstract
{
    public function transform(Player $player)
    {
        $user = $player->user()->get();

        return [
            'id' => $player->player_id,
            'name' => $player->player_nickname,
            'email' => $user[0]->email,
            'profile_picture' => $player->profile_picture,
            'player_rating' => $player->player_rating,
            'player_win' => $player->player_win,
            'player_loss' => $player->player_loss,
            'player_details' => $player->player_details,
            'created_at' => $player->created_at->toDateTimeString(),
            'matches' => $player->matches
        ];
 
    }
}