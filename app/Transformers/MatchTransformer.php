<?php

namespace App\Transformers;

use App\Models\Match;
use Illuminate\Support\Carbon;
use League\Fractal\TransformerAbstract;

class MatchTransformer extends TransformerAbstract
{
    public function transform(Match $match)
    {

//            I changed the return data but here is the past code.

//            'id' => $match->match_id,
//            'game_id' => $match->game_id,
//            'match_name' => $match->match_name,
//            'match_details' => $match->match_details,
//            'match_mode' => $match->match_mode,
//            'match_rules' => $match->match_rules,
//            'match_winner' => $match->match_winner,
//            'match_loser' => $match->match_loser,
//            'match_score' => $match->match_score,
//            'match_date' => $match->match_date,
//            'match_date_formatted' => date('Y/m/d',strtotime($match->match_date)),
//            'match_status' => $match->match_status,
//            'is_public' => $match->is_public,


//             "match_rules" => $match->find($match->match_id)->matchRules()->get(),
//            "match_participants" => $match->find($match->match_id)->matchParticipants()->get(),

        return [
            'id' => $match->match_id,
            "match_id" => $match->match_id,
            "game_id"=> $match->game_id,
            "game_details" => $match->gameDetails()->get(),
            "match_name" =>  $match->match_name,
            "match_venue" =>  "Valenzuela",
            "match_description" =>  "213131",
            "match_mode" =>  $match->match_mode,
            "match_winner" =>  $match->match_winner,
            "match_loser" =>  $match->match_loser,
            "match_score" =>  $match->match_score,
            "match_date" =>  $match->match_date,
            "server_time" => Carbon::now()->toDateTimeString(),
            'match_date_formatted' => Carbon::parse($match->match_date)->format('F d, Y - D'),
            "is_public" =>  $match->is_public,
            "match_status" =>   $match->match_status,
            "min_age" =>  $match->min_age,
            "max_age" =>  $match->max_age
        ];
    }
}