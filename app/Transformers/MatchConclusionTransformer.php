<?php

namespace App\Transformers;

use App\Models\MatchConclusion;
use League\Fractal\TransformerAbstract;

class MatchConclusionTransformer extends TransformerAbstract
{
    public function transform(MatchConclusion $match)
    {
        $match->with('match');

        return [
            'id'                    => $match->id,
            'match_id'              => $match->match->match_id,
            'name'                  => $match->match->match_name,
            'description'           => $match->match->match_description,
            'match_date'            => $match->match->match_date,
            'match_mode'            => $match->match->match_mode,
            'opponent_statement'    => $match->opponent_statement,
            'opponent_claim'        => $match->opponent_claim,
            'opponent_image'        => $match->opponent_image_attachment,
            'judgement_to_opponent' => $match->judgement_to_opponent,
            'creator_statement'     => $match->creator_statement,
            'judgement_to_creator'  => $match->judgement_to_creator,
            'creator_image'         => $match->creator_image_attachment,
            'creator_claim'         => $match->creator_claim,
            'created_at'            => $match->created_at->toDateTimeString(),
            'updated_at'            => $match->updated_at->toDateTimeString(),
        ];
    }
}
