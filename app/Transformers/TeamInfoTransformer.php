<?php

namespace App\Transformers;

use App\Models\TeamInfo;
use League\Fractal\TransformerAbstract;

class TeamInfoTransformer extends TransformerAbstract
{
    public function transform(TeamInfo $team)
    {
        $team->with('category');

        return [
            'team_id'          => $team->team_id,
            'creator'          => $team->team_leader,
            'name'             => $team->team_name,
            'category_name'    => $team->category->category_name,
            'category_image'   => $team->category->category_image,
            'category_id'      => $team->category_id,
            'game_id'          => $team->game->game_id,
            'game_name'        => $team->game->game_name,
            'image'            => $team->image,
            'background_image' => $team->background_image,
            'win'              => $team->team_win,
            'loss'             => $team->team_loss,
            'rating'           => $team->team_rating,
            'status'           => $team->status,
            'description'      => $team->team_description,
            'created_at'       => $team->created_at->toDateTimeString(),
            'updated_at'       => $team->updated_at->toDateTimeString(),
        ];
    }
}
