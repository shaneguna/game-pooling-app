<?php

namespace App\Listeners;

use App\Events\TeamChatMessage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TeamChatMessageListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FriendRequest  $event
     * @return void
     */
    public function handle(TeamChatMessage $event)
    {
        //
    }
}
