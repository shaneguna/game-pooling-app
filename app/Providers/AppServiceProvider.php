<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Helpers\FileManager\BaseManager;
use App\Http\Helpers\Utils\SocialUserResolver;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Way\Generators\GeneratorsServiceProvider::class);
            $this->app->register(\Xethron\MigrationsGenerator\MigrationsGeneratorServiceProvider::class);
        }

        $this->app->singleton('uploader', function ($app) {
            // For custom upload managers
            /*$config = config('filesystems.default', 'public');
            if ($config == 'cloud') {
                return new UploadManager();
            }*/
            return new BaseManager();
        });

        // $this->app->singleton(
        //     'Adaojunior\Passport\SocialUserResolverInterface',
        //     'App\Utils\SocialUserResolver');

        $this->app->singleton('Adaojunior\Passport\SocialUserResolverInterface', SocialUserResolver::class);
    }
}
