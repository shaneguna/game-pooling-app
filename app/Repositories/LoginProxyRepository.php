<?php

namespace App\Repositories;

use Illuminate\Foundation\Application;
use App\Repositories\UserRepository;
use App\Models\User;

class LoginProxyRepository
{
    const REFRESH_TOKEN = 'refreshToken';

    const INVALID_CREDENTIALS = [
        'code' => '405',
        'message' => 'Invalid Credentials'
    ];

    const INTERNAL_SERVER_ERROR = [
        'code' => '500',
        'message' => 'The server encountered an internal error, Please contact the server administrator.'
    ];

    const OAUTH_UNAUTHORIZED = [
        'code' => '401',
        'message' => 'Oauth Unauthorized'
    ];

    private $apiConsumer;

    private $auth;

    private $cookie;

    private $db;

    private $request;

    private $userRepository;


    public function __construct(Application $app, UserRepository $userRepository)
    {

        $this->userRepository = $userRepository;
        $this->apiConsumer = $app->make('apiconsumer');
        $this->auth = $app->make('auth');
        $this->cookie = $app->make('cookie');
        $this->db = $app->make('db');
        $this->request = $app->make('request');
    }


    /**
     * Attempt to create an access token using user credentials
     *
     * @param string $email
     * @param string $password
     */
    public function attemptLogin($email = null, $password = null, $type = null)
    {
        if (strcasecmp($type, 'player') == 0) {

            $user = User::select('users.*')
                ->where('email', $email)
                ->where('role', 'player')
                ->with('player')
                ->first();

            if ($user) {
                if (strcasecmp($user->role, $type) == 0) {
                    return $this->proxy('password', [
                        'username' => $email,
                        'password' => $password,
                        'user' => isset($user) ? $user : false,
                        'player' => isset($user->player) ? $user->player : false,
                    ]);
                }
            } else {
                return self::INVALID_CREDENTIALS;
            }


        } else {
            $user = User::where('email', $email)->first();
            if (!is_null($user) && (isset($user->role) && strcasecmp($user->role, $type) == 0)) {
                return $this->proxy('password', [
                    'username' => $email,
                    'password' => $password,
                    'user' => isset($user) ? $user : false
                ]);
            }
        }

        return self::INVALID_CREDENTIALS;
        // throw new InvalidCredentialsException();
    }


    /**
     * Attempt to create an access token using facebook user credentials
     *
     * @param string $email
     * @param string $password
     */
    public function facebookLogin($token, $email)
    {
        $user = User::select('users.*')
            ->where('email', $email)
            ->where('role', 'player')
            ->with('player')
            ->first();

        if (!is_null($token)) {
            return $this->proxy('social', [
                'access_token' => $token,
                'network' => 'facebook',
                'user' => isset($user) ? $user : false,
                'player' => isset($user->player) ? $user->player : false,
            ]);
        }

        return self::INVALID_CREDENTIALS;
        // throw new InvalidCredentialsException();
    }

    /**
     * Proxy a request to the OAuth server.
     *
     * @param string $grantType what type of grant type should be proxied
     * @param array $data the data to send to the server
     * @return array
     */
    public function proxy(string $grantType, array $data = [])
    {
        $name = [];
        $email = [];
        $role = [];
        $player = [];

        if (isset($data['user'])) {
            $name = $data['user']->first_name;
            $email = $data['user']->email;
            $role = $data['user']->role;
        }

        if (isset($data['player'])) {
            $player = $data['player'];
        }

        $data = array_merge($data, [
            'client_id' => config('app.client_id'),
            'client_secret' => config('app.client_secret'),
            'grant_type' => $grantType
        ]);

        $response = $this->apiConsumer->post('/oauth/token', $data);

        if ($response->status() == 401) {
            //if status code is 401 either invalid credentials or wrong OAUTH client ID or secret
            return self::INVALID_CREDENTIALS;
        }


        $data = json_decode($response->getContent());

        // Create a refresh token cookie
        $this->cookie->queue(
            self::REFRESH_TOKEN,
            $data->refresh_token,
            864000, // 10 days
            null,
            null,
            false,
            true // HttpOnly
        );

        return [
            'tokens' => [
                'access_token' => $data->access_token,
                'expires_in' => $data->expires_in,
                'refresh_token' => $data->refresh_token,
                'token_type' => $data->token_type
            ],
            'user' => [
                'name' => $name,
                'email' => $email,
                'role' => $role,
                'player' => $player
            ]
        ];
    }


    /**
     * Logs out the user. We revoke access token and refresh token.
     * Also instruct the client to forget the refresh cookie.
     */
    public function logout()
    {
        $accessToken = $this->auth->user()->token();

        $refreshToken = $this->db
            ->table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();

        $this->cookie->queue($this->cookie->forget(self::REFRESH_TOKEN));
    }

}