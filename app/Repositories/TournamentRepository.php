<?php

namespace App\Repositories;

use App\Models\Tournament;
use App\Models\TournamentContestant;
use App\Models\TournamentApplicant;
use App\Models\TeamInfo;


class TournamentRepository
{
    use BaseRepository;
    protected $model;



    public function __construct(Tournament $tournament)
    {
        $this->model = $tournament;
    }


    public function getTournamentDetails($tournament_id)
    {
    	$tournament_details =  $this->model->find($tournament_id);


        if(strcmp($tournament_details->type,'Team') === 0 )
        {
            return $tournament_details->with(['tournament_applicant.team','tournament_contestant.team'])->where('tournament_id',$tournament_id)->first();
        }else{
            return $tournament_details->with(['tournament_applicant.player','tournament_contestant.player'])->where('tournament_id',$tournament_id)->first();
        }
    }


    public function updateTournamentContestant($tournament_id,$data)
    {
    	/* Remove all the contestant based on tournament*/
    	TournamentContestant::where('tournament_id',$tournament_id)->delete();
    	
        $contestants = [];

        if($data['type'] === 'Team')
        {
            foreach($data['team_accepted'] as $key => $id)
            {
                $contestants[$key]['team_id'] = $id;
                $contestants[$key]['player_id'] = TeamInfo::find($id)->first()->team_leader;
                $contestants[$key]['tournament_id'] = $tournament_id;
                $contestants[$key]['created_at'] = date('Y-m-d: 00:00:00');
                $contestants[$key]['updated_at'] = date('Y-m-d: 00:00:00');
            }
        }

        return TournamentContestant::insert($contestants);
    }


    public function insertTournamentApplicant($data)
    {
        $applicant_exist = \DB::table('tournament_applicants')
        ->where('tournament_id',$data['tournament_id'])
        ->where('player_id',$data['player_id'])->count();

        $contestant_exist = \DB::table('tournament_contestants')
        ->where('tournament_id',$data['tournament_id'])
        ->where('player_id',$data['player_id'])->count();

        if($contestant_exist>0){
            return ['message'=> 'You already registered a team for this tournament', 'error'=> 404];
        }

        else if($applicant_exist>0){
            return ['message'=> 'You already have a pending request to join this tournament', 'error'=> 404];
        }

        \DB::table('tournament_applicants')->insert([
           'tournament_id' => $data['tournament_id'],
           'team_id'  => $data['team_id'],
           'player_id' => $data['player_id'],
           'created_at' => date('Y-m-d: 00:00:00'),
           'updated_at' => date('Y-m-d: 00:00:00') 
        ]);

        return ['message'=> 'You successfully send a request to join this tournament!', 'error'=> 200];
    }


    public function generateContestantsByTournamentID($tournament_id)
    {
        return \DB::table('team_infos')
        -> join('tournament_contestants','team_infos.team_id','=','tournament_contestants.team_id')
        -> where('tournament_id',$tournament_id)
        -> get();
    }

    public function updateTournamentImage($data)
    {
        \DB::table('tournaments')   
        ->where('tournament_id',$data['tournament_id'])
        ->update(['image'=>$data['tournament_image']]);
    }

}