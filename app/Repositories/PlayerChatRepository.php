<?php

namespace App\Repositories;

use App\Models\PlayerChat;
use App\Models\Player;
use App\Models\User;
use App\Events\PlayerMessaged;
use App\Notifications\MessageReceived;


class PlayerChatRepository
{
    use BaseRepository;
    protected $model;

    public function __construct(PlayerChat $playerChat)
    {
        $this->model = $playerChat;
    }

	/**
	 * Send chat message
	 * @param $request
	 * @return void
	*/
    public function sendMessage($request)
    {
        $array_ids = [
            $request['player_sender'],
            $request['player_receiver']   
        ];
        sort($array_ids);

        $request['channel'] = implode('-', $array_ids);
    	$playerChat = PlayerChat::create($request);
        $player_receiver = Player::find($request['player_receiver']);
        $user = User::find($player_receiver->user_id);
        $player_sender = Player::find($request['player_sender']);
        

        $data = [
            'name'=> 'New Message ',
            'title'=> $player_sender->player_nickname,
            'message' => 'You received a message',
            'shown' => false,
            'inset' => true,
            'divider' => true,
            'player_sender'=>$player_sender->player_id
        ];

        $user->notify(new MessageReceived($data));

        event(new PlayerMessaged($playerChat));     

        return $playerChat;
    }

    public function generatePlayerMessageSender($request)
    {
        $result = [];
        $player_id = $request['player_id'];


        $query1  =  \DB::table('player_chat')
        ->select('player_receiver')
        ->distinct()
        ->whereOr('player_receiver',$player_id)
        ->whereOr('player_sender',$player_id);

        $query2  =  \DB::table('player_chat')
        ->select('player_sender')
        ->distinct()
        ->whereOr('player_receiver',$player_id)
        ->whereOr('player_sender',$player_id)
        ->union($query1)
        ->get();

        foreach ($query2 as $key => $value) {
            if($value->player_sender <> $player_id){
                $newMessageCount = \DB::table('notifications')
                ->where('data->message_sent->player_sender',$value->player_sender)
                ->where('data->message_sent->shown',false)
                ->count();
                
                $result [$key]['player_id'] = $value->player_sender;
                $result [$key]['title'] = Player::find($value->player_sender)->player_nickname;
                $result [$key]['unread_messages'] = $newMessageCount;                
            }
        }
        
        return $result;
    }

    public function getPlayerMessages($request)
    {
        \DB::enableQueryLog();

        $result = [];
    	$query =  \DB::table('player_chat')
        ->where(function ($query) use($request) {
                $query->orWhere('player_sender',intval($request['player_sender']));
                $query->orWhere('player_receiver',intval($request['player_sender']));
        })
        ->where(function ($query) use($request) {
                $query->orWhere('player_sender',intval($request['player_id']));
                $query->orWhere('player_receiver',intval($request['player_id']));
        })
        ->orderBy('created_at')
        ->get();

        $updateNotification = \DB::table('notifications')
        ->where('data->message_sent->player_sender',$request['player_sender'])
        ->update(['data->message_sent->shown'=>true]);

        foreach ($query as $key => $chat) {
            $result[$key]['player_sender'] = Player::find($chat->player_sender)->player_nickname;
            $result[$key]['message'] = $chat->message;
            $result[$key]['player_receiver'] = $chat->player_receiver;
        }
    	return $result;
    }



}

