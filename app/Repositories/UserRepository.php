<?php

namespace App\Repositories;

// use App\Models\Permission;
// use App\Models\Role;
use App\Models\User;
// use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\Config;
use Request;
use DB;
use Input;
use  Image;

class UserRepository
{
    /**
     * @param  $params Parameter
     * @return
     */
    public function getUser($params)
    {
        return User::select('users.*')
        ->where(function ($query) use ($params) {
            if(isset($params['column'])){
               $query->where($params['column'],$params['operator'],$params['value']);
            }   
        })
        ->get();
    }
}