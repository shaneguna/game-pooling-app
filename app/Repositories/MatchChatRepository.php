<?php

namespace App\Repositories;

use App\Events\MatchChatEvent;
use App\Models\Match;
use App\Models\MatchChat as MatchChatModel;

class MatchChatRepository
{
    use BaseRepository;
    protected $model;

    public function __construct(MatchChatModel $matchChat)
    {
        $this->model = $matchChat;
    }

    /**
     * Send chat message
     * @param $request
     * @return void
     */
    public function matchChatMessage($request)
    {
        $matchChat = MatchChatModel::create($request);

        event(new MatchChatEvent($matchChat));

        return $matchChat;
    }

    public function isChatEnabled($match_id, $request)
    {
        if (isset($request['player_id'])) {

            $match = Match::where('match_id', $match_id)->with('player')->first()->toArray();

            if (strcmp($match['match_mode'], 'Team') > 0) {
                $match['player'][] = $match['created_by'];
                if (in_array($request['player_id'], $match)) {
                    return ['is_chat_enabled' => true];
                }
            } else {

                $count = 0;

                $count = \DB::table('team_infos')
                    ->join('match_team', 'match_team.team_id', '=', 'team_infos.team_id')
                    ->where('match_team.match_id', $match_id)
                    ->where('team_leader', $request['player_id'])
                    ->count();

                if ($count === 0) {
                    $count = \DB::table('team_infos')
                        ->join('match_team', 'match_team.team_id', '=', 'team_infos.team_id')
                        ->join('team_member', 'team_member.team_id', '=', 'team_infos.team_id')
                        ->where('team_member.player_id', $request['player_id'])
                        ->where('match_team.match_id', $match_id)
                        ->count();
                }

                if ($count > 0) {
                    return ['is_chat_enabled' => true];
                }
            }

        }

        return ['is_chat_enabled' => false];
    }

    public function getMatchMessages($request)
    {
        $matchChat = MatchChatModel::where('match_id', $request['match_id'])->with(['player', 'match'])->get();
//        $result = [];
        //
        //        $result []['header'] = 'Match Chat';
        //
        //        if(!$matchChat->isEmpty()){
        //            foreach ($matchChat as $key => $data) {
        //                $result[]['subtitle'] = "<span class='grey--text text--darken-2'> " . $data->player->player_nickname. " ". $data->message;
        //                $result[]['divider'] = true;
        //            }
        //        }

        return $matchChat;
    }

}
