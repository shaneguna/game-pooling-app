<?php

namespace App\Repositories;

use App\Models\Venue;

class VenueRepository
{
    use BaseRepository;
    protected $model;

    public function __construct(Venue $venue)
    {
        $this->model = $venue;
    }

    /**
     * Get record by the name.
     *
     * @param  string $name
     * @return collection
     */
    public function getByName($name)
    {
        return $this->model->where('venue_name', $name)->first();
    }

}