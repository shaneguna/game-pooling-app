<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\TeamChat;
use App\Events\TeamChatMessage;
use App\Notifications\TeamMessageReceived;


class TeamChatRepository
{
    use BaseRepository;
    protected $model;

    public function __construct(TeamChat $teamChat)
    {
        $this->model = $teamChat;
    }
	/**
	 * Send chat message
	 * @param $request
	 * @return void
	*/
    public function teamChatMessage($request)
    {
    	$teamChat = TeamChat::create($request);
        $teamMembers = [];

        foreach ($teamChat->team->members as $key => $value) {
            $teamMembers[$key] = $value->player_id;
        }

        $user = User::find($request['player_id']);

        $data = [
            'name'=> 'New Team Message',
            'title'=>   $teamChat->team->team_name,
            'message' => 'Team message received',
            'shown' => false,
            'inset' => true,
            'divider' => true,
            'team_id' => $request['team_id'],
            'player_sender'=>$request['player_id'],
            'team_members_unread' =>$teamMembers
        ];

        $user->notify(new TeamMessageReceived($data));
        
        event(new TeamChatMessage($teamChat));

        return $teamChat;
    }

    public function updateTeamNotif($request){
        $player_id = $request['player_id'];
        $team_notifications = \DB::table('notifications')
        ->whereNotNull('data->team_message_sent->team_id')
        ->get();
        foreach ($team_notifications as $key => $notif) {
            $json = json_decode($notif->data);            
            
            if(in_array($player_id,$json->team_message_sent->team_members_unread)){
              \DB::table('notifications')->where('id',$notif->id)->update(['data->team_message_sent->shown'=>true]);        
            }
        }        
           
    }

    public function getUnreadTeamChat($request)
    {
        $player_id = $request['player_id'];
        $team_id = $request['team_id'];
        $counter = 0;
        $team_notifications = \DB::table('notifications')
        ->where('data->team_message_sent->team_id',$request['team_id'])
        ->get();
        foreach ($team_notifications as $key => $notif) {
             $json = json_decode($notif->data);            

             if(in_array($player_id,$json->team_message_sent->team_members_unread)){
                    $counter = $counter+1;
             }
        }   

        return $counter;
    }


    public function getTeamMessages($request)
    {
    	$teamChat =  TeamChat::where('team_id',$request['team_id'])->with(['player','team'])->get();
    	// $player_id = $request['player_id'];
        $result = [];
        
        // $team_notifications = \DB::table('notifications')
        // ->where('data->team_message_sent->team_id',$request['team_id'])
        // ->get();

        // foreach ($team_notifications as $key => $notif) {
        //     $json = json_decode($notif->data);      
            
        //     $unread_members = $json->team_message_sent->team_members_unread;

        //     if (($key = array_search($player_id, $unread_members)) !== false) {
        //         unset($unread_members[$key]);
        //     }
            
        //     $unread_members = array_values($unread_members);
        //     $json->team_message_sent->team_members_unread = $unread_members;

        //     \DB::table('notifications')
        //     ->where('id',$notif->id)
        //     ->update(['data'=>json_encode($json)]);
        // }

        if(!$teamChat->isEmpty()){
            $result []['header'] = $teamChat[0]->team->team_name. ' Group Chat';

        	foreach ($teamChat as $key => $data) {
        		$result[]['subtitle'] = "<span class='grey--text text--darken-2'> " . $data->player->player_nickname. "</span> &mdash;". $data->message;
    			$result[]['divider'] = true;
        	}
        }

    	return $result;
    }

}

