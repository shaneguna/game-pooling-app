<?php

namespace App\Repositories;

use App\Models\Game;


class GameRepository
{
    use BaseRepository;
    protected $model;

    public function __construct(Game $game)
    {
        $this->model = $game;
    }

    /**
     * Get record by the name.
     *
     * @param  string $name
     * @return collection
     */
    public function getByName($name)
    {
        return $this->model->where('game_name', $name)->first();
    }

    public function getById($id)
    {
        return $this->model->where('game_id', $id)->first();
    }

    public function getByCategory($category_id)
    {
        return $this->model->where('category_id', $category_id)->get();
    }

    public function getByPage($offset, $limit){
        $take = $offset * $limit;
        return $this->model->skip($take)->take($limit)->get();
    }



    public function getSearchedResult($string)
    {
        /*return $this->model
            ->where('game_id', 'LIKE', "%{$string}%")
            ->orWhere('game_name', 'LIKE', "%{$string}%")
            ->join('categories', 'categories.category_id', '=', 'games.category_id')
            ->select(['games.*', 'categories.category_name'])
            ->get();*/

        return $this->model->where('game_name', 'LIKE' , "%{$string}%")
            ->orWhere('game_id', 'LIKE', "%{$string}%")
            ->with('category')
            ->get();
    }
}

