<?php

namespace App\Repositories;

use App\Events\MatchChatEvent;
use App\Models\Match;
use App\Models\MatchConclusion;


class MatchConclusionRepository
{
    use BaseRepository;
    protected $model;

    public function __construct(MatchConclusion $matchConclusion)
    {
        $this->model = $matchConclusion;
    }
}
