<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository
{
    use BaseRepository;
    protected $model;

    public function __construct(Category $category)
    {
        $this->model = $category;
    }

    /**
     * Get record by the name.
     *
     * @param  string $name
     * @return collection
     */
    public function getByName($name)
    {
        return $this->model->where('category_name', $name)->first();
    }

    public function getCategoriesWithGames($number = 10, $sort = 'desc', $sortColumn = 'created_at'){
        return $this->model->with('games')->orderBy($sortColumn, $sort)->paginate($number);
    }

    public function getByPage($offset, $limit){
        $take = $offset * $limit;
        return $this->model->skip($take)->take($limit)->get();
    }
}