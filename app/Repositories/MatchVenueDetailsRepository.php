<?php

namespace App\Repositories;

use App\Models\MatchVenueDetails;
use App\Models\Player;

class MatchVenueDetailsRepository
{
    use BaseRepository;
    protected $model, $player;

    public function __construct(MatchVenueDetails $match, Player $player)
    {
        $this->player = $player;
        $this->model = $match;
    }
}