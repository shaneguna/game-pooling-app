<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Player;

class SocialiteRepository
{
    public function createOrGetUser($data)
    {
        $email = isset($data['email']) ? $data['email'] : null;
        $exist = null;

        if ($email) {
            $exist = \DB::table('users')->where('email', $email)->count();
        }

        if ($exist == 0 && $email) {
            $user = User::create([
                'email' => $email,
                'name' => isset($data['name']) ? $data['name'] : '-',
                'first_name' => isset($data['name']) ? $data['name'] : '-',
                'gender' => isset($data['gender']) ? $data['gender'] : 'Male',
                'login_type' => 'socialite',
                'role' => 'Player',
                'password' => ' ',
            ]);

            $this->createPlayer($user);
        }
    }

    public function createPlayer($userData)
    {
        $playerData = [];
        $playerData['name'] = $userData->name;
        $playerData['player_nickname'] = $userData->name;
        $playerData['login_type'] = 'passport';
        $playerData['role'] = 'player';
        $playerData['player_rating'] = '0';
        $playerData['player_win'] = '0';
        $playerData['player_loss'] = '0';
        $playerData['player_details'] = '';
        $playerData['user_id'] = $userData->id;

        Player::create($playerData);
    }
}