<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Player;

class SocialNetworkRepository
{
    use BaseRepository;
    protected $model, $player;

    public function __construct(User $user, Player $player)
    {
        $this->model = $user;
        $this->player = $player;
    }

    /**
     * Get record by the name.
     *
     * @param  string $name
     * @return collection
     */
    public function getByName($name)
    {
        return $this->model->where('user_name', $name)->first();
    }

    /**
     * Get record by role.
     *
     * @param  string $name
     * @return collection
     */
    public function getUserByRole($role)
    {
        return $this->model->where('role', $role)->paginate(10);
    }

    public function addFriend($recipient)
    {
        $recipient_user_id = $this->player->find($recipient);

        $recipient = $this->model->find($recipient_user_id->user_id);

        $sender = auth()->guard('api')->user();
        $be_friend = $sender->befriend($recipient);

        return $be_friend;
    }

    public function getFriends($id)
    {
        $player = $this->player->find($id);
        $user = $this->model->find($player->user_id);
        $friends = $user->getFriends();
        $return_data = collect( );

        $friends->each(function ($item) use ($return_data)
        {
            $return_data->push($item->player()->select('player_id as id','player_nickname as name')->first());
        });

        return $return_data;
    }

    public function getPendingRequests($id)
    {
        $player = $this->player->find($id);
        $user = $this->model->find($player->user_id);
        return $user->getPendingFriendships()->load('recipient')->load('sender');
    }

    public function acceptFriendRequest($id){

        $sender = $this->model->find($id);
        $recipient = auth()->guard('api')->user();

        $accept_friend = $recipient->acceptFriendRequest($sender);

        return $accept_friend;
    }


}