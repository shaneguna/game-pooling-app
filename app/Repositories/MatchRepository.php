<?php

namespace App\Repositories;

use App\Models\Match;
use App\Models\MatchConfirmation;
use App\Models\MatchInvite;
use App\Models\Player;
use Carbon\Carbon;

class MatchRepository
{
    use BaseRepository;
    protected $model, $player, $socialNetworkRepo, $teamRepo, $playerRepo;

    public function __construct(Match $match, Player $player, SocialNetworkRepository $socialNetworkRepo, TeamRepository $teamRepo, PlayerRepository $playerRepo)
    {
        $this->player            = $player;
        $this->model             = $match;
        $this->socialNetworkRepo = $socialNetworkRepo;
        $this->teamRepo          = $teamRepo;
        $this->playerRepo        = $playerRepo;
    }

    public function getListMatchesWithDistinction($arguments)
    {
        $player_id = $arguments['player_id'];
        $game_id   = (isset($arguments['game_id']) && !empty($arguments['game_id'])) ? $arguments['game_id'] : 0;

        $matchList['player_matches'] = $this->playerTeamMatches($player_id, $game_id);
        $matchList['global_matches'] = $this->matchesPlayerTeamNotLinked(array_column($matchList['player_matches'], 'match_id'));

        return $matchList;
    }

    public function playerTeamMatches(int $player_id = 0, int $game_id = 0)
    {

        $teamsLeadByPlayer = \DB::table('team_infos')->select('team_id')
            ->where('team_leader', $player_id)->get()
            ->toArray();

        $teamsByPlayer = \DB::table('team_infos')->select('team_infos.team_id')
            ->join('team_member', 'team_member.team_id', '=', 'team_infos.team_id')
            ->where('team_member.player_id', $player_id)
            ->where('team_infos.team_leader', '<>', $player_id)
            ->get()->toArray();

        $teams   = array_merge($teamsLeadByPlayer, $teamsByPlayer);
        $teamIDs = array_column($teams, 'team_id');

        $playerTeamMatches = \DB::table('matches')
            ->join('match_team', 'match_team.match_id', '=', 'matches.match_id')
            ->whereIn('match_team.team_id', $teamIDs)
            ->where('match_mode', 'Team')
            ->when($game_id > 0, function ($query) use ($game_id) {
                $query->where('game_id', $game_id);
            })
        // ->where('match_status', '<>', 'Match Over')
        // ->where('match_status', '<>', 'Match Cancelled')
            ->get()->toArray();

        $playerMatches = \DB::table('matches')
            ->where('match_mode', 'Solo')
            ->join('match_player', 'match_player.match_id', '=', 'matches.match_id')
            ->where('match_player.player_id', $player_id)
            ->when($game_id > 0, function ($query) use ($game_id) {
                $query->where('game_id', $game_id);
            })
            ->where('match_status', '<>', 'Match Over')
            ->where('match_status', '<>', 'Match Cancelled')
            ->get()->toArray();

        return array_merge($playerTeamMatches, $playerMatches);
    }

    public function matchesPlayerTeamNotLinked($match_ids = [], $game_id = 0)
    {
        return \DB::table('matches')
            ->where('match_status', '<>', 'Match Over')
            ->when($game_id > 0, function ($query) use ($game_id) {
                $query->where('game_id', $game_id);
            })
            ->where('match_status', '<>', 'Match Over')
            ->where('match_status', '<>', 'Match Cancelled')
            ->WhereNotIn('match_id', $match_ids)
            ->get()->toArray();
    }

    public function matchesPlayerInvitesOrRequests($player_id, $game_id)
    {
        return \DB::table('matches')
            ->join('match_invites', 'match_invites.match_id', '=', 'matches.match_id')
            ->join('match_player', 'match_player.match_id', '=', 'matches.match_id')
            ->where('match_mode', 'Solo')
            ->when($game_id > 0, function ($query) use ($game_id) {
                $query->where('game_id', $game_id);
            })
            ->where('match_invites.player_id', '<>', $player_id)
            ->where('match_player.player_id', '<>', $player_id)
            ->where('match_status', '<>', 'Match Over')
            ->where('match_status', '<>', 'Match Cancelled')
            ->get()->toArray();
    }

    public function teamsMatchInvitesOrRequests($player_id, $game_id)
    {
        $teamsLeadByPlayer = \DB::table('team_infos')->select('team_id')
            ->where('team_leader', $player_id)->get()
            ->toArray();

        $teamsByPlayer = \DB::table('team_infos')->select('team_infos.team_id')
            ->join('team_member', 'team_member.team_id', '=', 'team_infos.team_id')
            ->where('team_member.player_id', $player_id)
            ->where('team_infos.team_leader', '<>', $player_id)
            ->get()->toArray();

        $teams   = array_merge($teamsLeadByPlayer, $teamsByPlayer);
        $teamIDs = array_column($teams, 'team_id');

        $matches = \DB::table('matches')
            ->join('match_invites', 'match_invites.match_id', '=', 'matches.match_id')
            ->where('match_mode', 'Team')
            ->whereIn('match_invites.team_id', $teamIDs)
            ->when($game_id > 0, function ($query) use ($game_id) {
                $query->where('game_id', $game_id);
            })
            ->where('match_status', '<>', 'Match Over')
            ->where('match_status', '<>', 'Match Cancelled')
            ->distinct()
            ->get()->toArray();

        $results = [];

        foreach ($matches as $key => $match) {
            $results[$match->match_id] = $match;
        }

        return array_values($results);
    }

    /**
     * Get record by the name.
     *
     * @param  string $name
     * @return collection
     */
    public function getByName(string $name)
    {
        return $this->model->where('match_name', $name)->first();
    }

    /**
     * Get records by the game.
     *
     * @param $id
     * @return mixed
     */
    public function getAllByGameId(string $id)
    {
        return $this->model->where('game_id', $id)->globalMatches()->orderBy('created_at', 'desc')->get();
    }

    /**
     *
     * @param $input
     * @return mixed
     */
    public function joinMatch($input)
    {
        $match = $this->model->find($input->match_id);
        $match->player()->attach($input->player_id);
        return $this->getByPlayerId($input->player_id);
    }

    public function matchesByVicinity($id)
    {
        return $this->model->where('game_id', $id)->globalMatches()->get();
    }

    public function getByPage($offset, $limit)
    {
        $take = $offset * $limit;
        return $this->model->skip($take)->take($limit)->get();
    }

    /**
     * Get records linked to player id.
     *
     * @param string $id
     * @param int $limit
     * @return mixed
     */
    public function getByPlayerId(string $id, int $limit)
    {
        $team_match_ids = \DB::table('matches')
            ->select('matches.match_id')
            ->join('match_team', 'match_team.match_id', '=', 'matches.match_id')
            ->join('team_member', 'team_member.team_id', '=', 'match_team.team_id')
            ->where('team_member.player_id', $id)
            ->get()
            ->toArray();
        $team_match_ids = array_column($team_match_ids, 'match_id');

        $player_match_ids = \DB::table('matches')
            ->select('matches.match_id')
            ->join('match_player', 'match_player.match_id', '=', 'matches.match_id')
            ->where('match_player.player_id', $id)
            ->get()
            ->toArray();
        $player_match_ids = array_column($player_match_ids, 'match_id');

        $match_ids = array_merge($player_match_ids,$team_match_ids);

        return $this->model->whereIn('match_id',$match_ids)->get();

        // if ($id > 0) {
        //     $player = $this->player->find($id);
        //     if ($limit && $limit > 0) {
        //         return $player->matches->sortByDesc('match_date')->take($limit);
        //     } else {
        //         return $player->matches->sortByDesc('match_date');
        //     }
        // }

        // return [];
    }

    public function getByPlayerAndGameId(string $id, int $limit, int $game)
    {
        $player = $this->player->find($id);

        $match_ids_team_creator = \DB::table('matches')
            ->select('matches.match_id')
            ->join('match_team', 'match_team.match_id', '=', 'matches.match_id')
            ->join('team_member', 'team_member.team_id', '=', 'match_team.team_id')
            ->where('team_member.player_id', $id)
            ->where('game_id', $game)
            ->get()
            ->toArray();

        $match_ids_opponent = \DB::table('matches')
            ->select('matches.match_id')
            ->join('match_invites', 'match_invites.match_id', '=', 'matches.match_id')
            ->join('team_member', 'team_member.team_id', '=', 'match_invites.team_id')
            ->where('team_member.player_id', $id)
            ->where('game_id', $game)
            ->get()
            ->toArray();

        $match_ids = array_merge($match_ids_team_creator, $match_ids_opponent);

        $ids = [];

        foreach ($match_ids as $key => $match) {
            $ids[] = $match->match_id;
        }

        return $this->model->whereIn('match_id', $ids)->globalMatches()->orderBy('created_at', 'desc')->get();

        // if ($limit && $limit > 0) {
        //     return $player->matches->where('game_id', $game)->take($limit);
        // } else {
        //     return $player->matches->where('game_id', $game)->sortByDesc('match_date');
        // }

    }

    /**
     * Get records of games which are globally visible.
     *
     * @return mixed
     */
    public function getAllGlobalMatches()
    {
        return $this->model->globalMatches()->get();
    }

    public function matchRulesByMatchId($id)
    {
        return $this->model->find($id)->matchRules()->get();
    }

    public function matchParticipantsByMatchId(int $id, string $mode)
    {
        if ($mode == 'Team') {
            return $this->model->find($id)->matchTeams()->get();
        } else if ($mode = 'Solo') {
            return $this->model->find($id)->matchPlayer()->get();
        }

    }

    public function formulatePossibleMatchPlayers(int $player_id, int $game_id, string $match_mode)
    {
        $return = [
            'player_consumption'   => ['singular' => [], 'teams' => []],
            'opponent_consumption' => ['singular' => [], 'teams' => []],
        ];

        if ($match_mode == 'Solo' || $match_mode == 'Solo Mode') {
            // singular for team members
            $player_friends             = $this->socialNetworkRepo->getFriends($player_id)->prepend((object) array('header' => 'Friends'));
            $other_players_favored_game = $this->playerRepo->getPublicPlayersByFavoriteGame($game_id, $player_id)->prepend((object) array('header' => 'Other Players Interested in this Game'));

            $merge_player_consumption = collect($player_friends)->merge($other_players_favored_game->toArray());

            // opponent singular consumption
            $non_friendlies = $this->playerRepo->getAllPlayers($game_id, $player_id)->prepend((object) array('header' => 'Other Players'));

            // assign data set
            $return['player_consumption']['singular']   = $merge_player_consumption;
            $return['opponent_consumption']['singular'] = $non_friendlies;

        } else {
            // my team consumption
            $player_team = $this->teamRepo->teamsByPlayerAndGame($player_id, $game_id)->prepend((object) array('header' => 'My Teams'));
            // opponent team consumption
            $teams_by_game = $this->teamRepo->teamsByGame($player_id, $game_id)->prepend((object) array('header' => 'Other Teams Interested in this Game'));

            // assign data set
            $return['player_consumption']['teams']   = $player_team;
            $return['opponent_consumption']['teams'] = $teams_by_game;
        }

        return $return;
    }

    public function matchParticipants($match_id)
    {
        $match             = Match::where('match_id', $match_id)->with('player')->first()->toArray();
        $match['player'][] = $match['created_by'];
        return ['players' => $match['player']];
    }

    public function sendRequestToJoinMatch(array $arguments = [])
    {

        $alreadyAcceptedATeam = MatchInvite::where('match_id', $arguments['match_id'])
            ->where('type', 'Approved')
            ->count();

        if ($alreadyAcceptedATeam > 0) {
            return ['error' => true, 'message' => 'The match creator already accepted an opponent'];
        }

        if ($arguments['team_id'] > 0) {

            $exist = \DB::table('match_invites')
                ->join('matches', 'matches.match_id', '=', 'match_invites.match_id')
                ->where('matches.match_id', $arguments['match_id'])
                ->where('match_invites.match_id', $arguments['match_id'])
                ->where('match_invites.team_id', $arguments['team_id'])
                ->get()
                ->toArray();

            if (count($exist) > 0 && $exist[0]->invite_mode === 'invited') {
                return ['error' => true, 'message' => 'Your team were already invited to join this match.'];
            }

            if (count($exist) > 0 && $exist[0]->type === 'Reject') {
                return ['error' => true, 'message' => 'Your request were rejected.'];
            }

            if (count($exist) > 0 && $exist[0]->type === 'Approved') {
                return ['error' => true, 'message' => 'Your team already belong to this match.'];
            }

            if (count($exist) > 0 && $exist[0]->type === 'Pending') {
                return ['error' => true, 'message' => 'Your team already requested to join this match and currently in pending.'];
            }
        }

        \DB::table('match_invites')->insert($arguments);

        return ['error' => false, 'message' => 'Request to join this match sent'];
    }

    public function generateMatchDetails(array $arguments = [])
    {

        $query = $this->model
            ->with([
                'gameDetails',
                'matchRules',
                'matchInvites.teamInfo.teamLeader',
                'matchTeams.teamLeader',
                'matchTeams.members',
                'matchPlayer',
                'matchVenueDetails',
                'matchConfirmation.opponent_team_info',
                'matchConfirmation.opponent_player',
                'matchConclusion',
            ])
            ->where('match_id', $arguments['match_id'])
            ->first()
            ->toArray();

        if ($query['match_mode'] === 'Team') {
            unset($query['match_player']);
            $query['team_creator'] = $query['match_teams'][0];
            unset($query['match_teams']);
        } else {
            unset($query['match_teams']);
            $query['player_creator'] = $query['match_player'];
            unset($query['match_player']);
        }

        $query['match_date_formatted'] = Carbon::parse($query['match_date'])->format('F d, Y - D');

        return $query;
    }

    public function createMatchConfirmation(array $arguments = [])
    {
        $match = $this->model->find($arguments['match_id'])->toArray();

        $match_confirmation = [
            'match_id'           => $match['match_id'],
            'team_creator_id'    => $match['team_creator'],
            'player_creator_id'  => $match['player_creator'],
            'team_opponent_id'   => $arguments['team_id'],
            'player_opponent_id' => $arguments['player_id'],
            'creator_status'     => 'no_confirmation',
            'opponent_status'    => 'no_confirmation',
        ];

        MatchConfirmation::create($match_confirmation);
    }

    public function updateInviteStatus(array $arguments = [])
    {
        if ($arguments['type'] === 'Approved') {
            $this->createMatchConfirmation($arguments);
        }

        $update = MatchInvite::where('match_id', $arguments['match_id'])
            ->where('team_id', $arguments['team_id'])
            ->where('player_id', $arguments['player_id'])
            ->update(['type' => $arguments['type']]);
    }

    public function updateMatchConfirmation(array $arguments = [])
    {
        $opponent      = $arguments['opponent'];
        $match_creator = $arguments['ally'];

        if (!empty($opponent)) {

            if ($opponent['respond'] === 'cancel') {
                \DB::table('match_confirmations')
                    ->where('match_id', $arguments['match_id'])
                    ->update(['team_opponent_id' => 0, 'player_opponent_id' => 0]);

                \DB::table('match_invites')
                    ->where('match_id', $arguments['match_id'])
                    ->where('team_id', $opponent['team_id'])
                    ->where('player_id', $opponent['player_id'])
                    ->update(['type' => 'Reject']);
            }

            if ($opponent['respond'] === 'confirmed') {

                \DB::table('match_confirmations')
                    ->where('match_id', $arguments['match_id'])
                    ->update(['opponent_status' => 'confirmed']);
            }
        }

        if (!empty($match_creator)) {

            if ($match_creator['respond'] === 'cancel') {
                \DB::table('match_confirmations')
                    ->where('match_id', $arguments['match_id'])
                    ->update(['team_opponent_id' => 0, 'player_opponent_id' => 0, 'opponent_status' => 'no_confirmation']);

                // All the  request to join and invitation will be back to pending status for approval
                \DB::table('match_invites')
                    ->where('match_id', $arguments['match_id'])
                    ->update(['type' => 'Pending']);
            }

            if ($match_creator['respond'] === 'confirmed') {

                \DB::table('match_confirmations')
                    ->where('match_id', $arguments['match_id'])
                    ->update(['creator_status' => 'confirmed']);
            }
        }
    }

}
