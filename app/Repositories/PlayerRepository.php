<?php

namespace App\Repositories;

use App\Models\Player;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Repositories\SocialNetworkRepository;

class PlayerRepository
{
    use BaseRepository;
    protected $model;

    const EMAIL_ALREADY_TAKEN = [
        'code' => 409,
        'message' => 'Email has already been taken'
    ];
    const CREATE_SUCCESS = [
        'code' => 201,
        'message' => 'create success',
        'data' => ''
    ];
    const CREATE_UNSUCCESSFUL = [
        'code' => 400,
        'message' => 'create unsuccessful'
    ];

    public function __construct(Player $player)
    {
        $this->model = $player;
    }

    /**
     * Get record by the name.
     *
     * @param  string $name
     * @return collection
     */
    public function getByName($name)
    {
        return $this->model->where('match_name', $name)->first();
    }

    /**
     * Get record by the id.
     *
     * @param  string $id
     * @return collection
     */
    public function getByPlayerID($id)
    {
        return $this->model->where('player_id', $id)->first();
    }

    public function getByPage($offset, $limit)
    {
        $take = $offset * $limit;
        return $this->model->skip($take)->take($limit)->get();
    }

    /**
     * Create user to be used in creation of player
     *
     * @param  array $request
     * @return collection
     */
    public function createUser($request)
    {

        $checkEmailExistence = User::where('email', $request->get('email'))->get();
        if(count($checkEmailExistence) > 0){
            return self::EMAIL_ALREADY_TAKEN;
        }else{

            $userData = $request->all();
            $userData['password'] = Hash::make($request->get('password'));
            $userData['name'] = $request->get('first_name') . ' ' . $request->get('last_name');
            $userData['login_type'] = 'passport';
            $userData['role'] = 'player';
            $userData['player_rating'] = '0';
            $userData['player_win'] = '0';
            $userData['player_loss'] = '0';
            $userData['player_details'] = '';

            $newUser = User::firstOrCreate(['email' => $request->get('email')], $userData);

            //If user was successfully created return user
            if($newUser->wasRecentlyCreated){
                $userData['user_id'] = $newUser->id;
                $userData['player_nickname'] = $request->get('name');

                $res = self::CREATE_SUCCESS;
                $res['data'] = $userData;

                return $res;
            }else{
                //user was not created
                self::CREATE_UNSUCCESSFUL;
            }
        }

    }


    /**
     * Create player
     *
     * @param  array $userData
     * @return void
     */
    public function createPlayer($request)
    {
        $userData = $this->createUser($request);

        if($userData['code'] == 201){
            $this->model->create($userData['data']);

            $userData['data'] = '';
            return $userData;
        }
        return $userData;
    }


    /**
     * Get list of player_id by team id.
     *
     * @param  string $name
     * @return collection
     */
    public function getUserByRole($role)
    {
        return $this->model->where('role', $role)->paginate(10);
    }

    /**
     * Get List of Players by team id
     *
     * @param  int $player_id
     * @return collection
     */
    public function playersByTeamID($team_id)
    {
         return Player::whereIn('player_id', $this->getPlayersByTeamID($team_id))->with('user')->get();
    }


    public function getPlayersByTeamID($team_id)
    {
        $result = [];

        $player_ids = \DB::table('team_member')->where('team_id', $team_id)->select('player_id')->get();

        foreach ($player_ids as $key => $value) {
            $result[$key] = $value->player_id;
        }
        return $result;

    }

    public function getFavoriteGames(int $player_id)
    {
        return $this->model->find($player_id)->playerGameFavorites()->get();
    }

    public function getPublicPlayersByFavoriteGame(int $game_id, int $player_id)
    {
        return $players = $this->model->whereHas('playerGameFavorites', function ($q) use ($game_id) {
            $q->where('games.game_id', $game_id);
        })->publicProfile()->where('player_id', '!=', $player_id)->select('player_id as id', 'player_nickname as name')->get();
    }

    public function getAllPlayers(int $game_id, int $player_id)
    {
        return $players = $this->model->where('player_id','<>',$player_id)
        ->select('player_id as id', 'player_nickname as name')->get();
    }


    public function generateTeamsLeadByPlayer($player_id)
    {
        $player = $this->model->where('player_id',$player_id)->with('teams_owned')->first();
        return $player->teams_owned;
    }
}


