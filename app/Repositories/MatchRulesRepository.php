<?php

namespace App\Repositories;

use App\Models\MatchRules;
use App\Models\Player;

class MatchRulesRepository
{
    use BaseRepository;
    protected $model, $player;

    public function __construct(MatchRules $matchRules, Player $player)
    {
        $this->player = $player;
        $this->model = $matchRules;
    }
}