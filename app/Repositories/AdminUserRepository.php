<?php

namespace App\Repositories;

use App\Models\User;

class AdminUserRepository
{
    use BaseRepository;
    protected $model;

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    /**
     * Get record by the name.
     *
     * @param  string $name
     * @return collection
     */
    public function getByName($name)
    {
        return $this->model->where('user_name', $name)->first();
    }

    /**
     * Get record by role.
     *
     * @param  string $name
     * @return collection
     */
    public function getUserByRole($role)
    {
        return $this->model->where('role', $role)->paginate(10);
    }


}