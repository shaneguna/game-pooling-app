<?php

namespace App\Repositories;

use App\Models\Game;
use App\Models\Player;
use App\Models\Category;
use App\Models\TeamInfo;

class SearchRepository
{
    use BaseRepository;
    protected $model;

    public function __construct(Game $game, Category $category, Player $player, TeamInfo $teaminfo)
    {
        $this->game = $game;
        $this->category = $category;
        $this->player = $player;
        $this->teaminfo = $teaminfo;
    }

    /**
     * Get record by game.
     *
     * @param  string $name
     * @return collection
     */
    public function searchByGame($string)
    {
        return $this->game
            ->where('game_name', 'LIKE' , "%{$string}%")
            ->orWhere('game_id', 'LIKE', "%{$string}%")
            ->with('category')
            ->get();
    }

    /**
     * Get record by category.
     *
     * @param  string $name
     * @return collection
     */

    public function searchByCategory($string)
    {
        return $this->category
            ->where('category_name', 'LIKE' , "%{$string}%")
            ->orWhere('category_description', 'LIKE', "%{$string}%")
            ->get();
    }

    /**
     * Get records by player.
     *
     * @param  string $name
     * @return collection
     */

    public function searchByPlayer($string)
    {
        return $this->player
            ->where('player_nickname', 'LIKE' , "%{$string}%")
            ->orWhere('player_id', 'LIKE', "%{$string}%")
            ->orWhere('player_details', 'LIKE', "%{$string}%")
            ->get();
    }

     public function searchByTeam($string)
    {
        return $this->teaminfo
            ->where('team_name', 'LIKE' , "%{$string}%")
            ->get();
    }
}

