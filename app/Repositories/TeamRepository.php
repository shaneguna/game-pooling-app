<?php

namespace App\Repositories;

use App\Models\TeamInfo;
use App\Models\Player;
use App\Models\Teaminvite;


class TeamRepository
{
    use BaseRepository;
    protected $model;

    public function __construct(TeamInfo $team)
    {
        $this->model = $team;
    }

    /**
     * Get record by the name.
     *
     * @param  string $name
     * @return collection
     */
    public function getByName($name)
    {
        return $this->model->where('match_name', $name)->where('is_active', 1)->first();
    }

    /**
     * Get record by the player id.
     *
     * @param  integer $player_id
     * @return collection
     */
    public function getByPlayerID($player_id)
    {
        return $this->model->where('team_player', $player_id)->where('is_active', 1)->get();
    }

    /**
     * Create team with fields that has a default value.
     *
     * @param  string $name
     * @return collection
     */
    public function createTeamDefault($data)
    {
        $player = Player::find($data['team_leader']);

        $data['team_win'] = 0;
        $data['team_loss'] = 0;
        $data['status'] = '';
        $data['accept_request'] = 0;
        $data['team_rating'] = 0;
        $data['is_active'] = 1;

        $team = $this->model->create($data);
        $player->teams()->attach($team->team_id);

        return $team;
    }

    /**
     * Create temporary team with fields that has a default value.
     *
     * @param $data
     * @param $match
     * @param $param
     * @return mixed
     */
    public function createTeamTemporary($data, $match, $param)
    {
        $data['team_win'] = 0;
        $data['team_loss'] = 0;
        $data['status'] = '';
        $data['team_rating'] = 0;
        $data['is_temporary'] = 1;

        $team = $this->model->create(array_merge($data, $param));

        foreach ($data['players'] as $player) {
            $player = Player::find($player['id']);
            $player->teams()->attach($team->team_id);
            // include match_player attachment to lessen loops
            $match->player()->attach($player['id']);
        }

        return $team;
    }

    /**
     * Invite a player.
     *
     * @param  int $player_id
     * @param  int $team_id
     * @return collection
     */
    public function invitePlayer($nickname, $team_id)
    {
        $player = Player::where('player_nickname', $nickname)->first();


        if ($player) {

            $exist = \DB::table('team_member')
            ->where('player_id', $player->player_id)
            ->where('team_id', $team_id)
            ->count();

            if (!$exist) {
                $insert = \DB::table('team_member')->insert([
                    'team_id' => $team_id,
                    'player_id' => $player->player_id
                ]);

                return array("message" => "Player added!", "code" => 200);
            }

            return array("message" => "Player already a member", "code" => 405);
        }

        return array("message" => "Player nickname does not exist", "code" => 404);
        // return Teaminvite::create([
        //     'team_id'   => $team_id,
        //     'player_id' => $player->id,
        //     'status'    => 'Pending',
        //     'mode'      => 'team invite'
        // ]);
    }

    public function teamListInvitation($team_id)
    {
        return TeamInvite::with('player')->where('team_id', $team_id)->where('status', 'Pending')->get();
    }

    /**
     * Request to join a team.
     *
     * @param  int $player_id
     * @param  int $team_id
     * @return collection
     */
    public function joinTeamRequest($player_id, $team_id)
    {
        return Teaminvite::create([
            'team_id' => $team_id,
            'player_id' => $player_id,
            'status' => 'Pending',
            'mode' => 'player invite'
        ]);
    }


    /**
     * Accept Invitation
     *
     * @param  int $player_id
     * @param  int $team_id
     * @return collection
     */
    public function acceptInvitation($player_id, $team_id)
    {

        $team = Teaminfo::find($team_id);
        $player = Player::find($player_id);
        $team->members()->attach($player);

        return Teaminvite::where('player_id', $player_id)
            ->where('team_id', $team_id)
            ->update(['status' => 'Accepted']);
    }

    /**
     * Reject Invitation
     *
     * @param  int $player_id
     * @param  int $team_id
     * @return collection
     */
    public function rejectInvitation($player_id, $team_id)
    {
        return Teaminvite::where('player_id', $player_id)
            ->where('team_id', $team_id)
            ->update(['status' => 'Rejected']);
    }


    /**
     * Get List of Teams by player id
     *
     * @param  int $player_id
     * @return collection
     */
    public function teamsByPlayer($player_id)
    {
        return TeamInfo::whereIn('team_id', $this->getTeamIDsByPlayerID($player_id))->with('category')->get();
    }

    public function teamsByGame(int $player_id, int $game_id)
    {
        return $this->model
            ->whereNotIn('team_id', $this->getTeamIDsByPlayerID($player_id))
            ->where('game_id', $game_id)
            ->where('team_leader','<>',$player_id)
            // ->notTemporary()
            // ->where('is_active', 1)
            ->select('team_id as id', 'team_name as name')
            ->get();
    }

    public function teamsByPlayerAndGame(int $player_id, int $game_id)
    {
        return TeamInfo::select('team_id as id', 'team_name as name')
            // ->where('is_active', 1)
            // ->whereIn('team_id', $this->getTeamIDsByPlayerID($player_id))
            ->where('team_leader',$player_id)
            ->where('game_id', $game_id)
            // ->notTemporary()
            ->get();
    }

    /**
     * Get name by team id
     *
     * @param  int $team_id
     * @return collection
     */
    public function teamNameById($team_id)
    {
        return $this->model->findorFail($team_id);
    }


    /**
     * @param $player_id
     * @return array
     */
    public function getTeamIDsByPlayerID($player_id)
    {
        $result = [];
        $team_ids = \DB::table('team_member')
            ->where('player_id', $player_id)
            ->select('team_infos.team_id')
            ->join('team_infos', 'team_member.team_id', '=', 'team_infos.team_id')
            ->where('team_member.is_active', 1)
            ->get();

        foreach ($team_ids as $key => $value) {
            $result[$key] = $value->team_id;
        }
        return $result;
    }

    /**
     * Get Teams Matches
     *
     * @param  int $team_id
     * @return collection
     */
    public function getTeamMatches($team_id)
    {
        $matches = [];
        $query = \DB::table('matches')
            ->join('match_team', 'match_team.match_id', '=', 'matches.match_id')
            ->join('match_rules', 'match_rules.match_id', '=', 'matches.match_id')
            ->where('match_team.team_id', $team_id)
            ->get();

        foreach ($query as $key => $match) {
            $matches [$key] ['match_name'] = $match->match_name;
            $matches [$key] ['mode'] = $match->match_mode;
            $matches [$key] ['winner'] = $match->match_winner;
            $matches [$key] ['losser'] = $match->match_loser;
            $matches [$key] ['score'] = $match->match_score;
            $matches [$key] ['date'] = $match->match_date;
            $matches [$key] ['status'] = $match->match_status;
        }

        return $matches;
    }

    public function getTeamsByGame(int $game_id)
    {
        return $this->model->where('game_id', $game_id)->where('is_active', 1)->get();
    }

    public function getTeamMembers(int $team_id)
    {
        $team = $this->teamNameById($team_id);
        return $team->members;
    }

    public function leaveTeamByPlayerID($data)
    {
        return \DB::table('team_member')->where('team_id', $data['team_id'])->where('player_id', $data['player_id'])->delete();
    }

    public function getTeamList($show_inactive = false)
    {
        $teams = TeamInfo::with(['category', 'team_leader_info', 'members'])->orderBy('created_at');

        if ($show_inactive === false) {
            $teams->where('is_active', 1);
        }

        $results = [];

        foreach ($teams->get() as $key => $value) {
            $results[$key]['row_number'] = $key + 1;
            $results[$key]['team_id'] = $value->team_id;
            $results[$key]['name'] = $value->team_name;
            $results[$key]['team_leader'] = $value->team_leader_info->player_nickname;
            $results[$key]['category_name'] = $value->category->category_name;
            $results[$key]['category_image'] = $value->category->category_image;
            $results[$key]['description'] = $value->team_description;
            $results[$key]['status'] = $value->status;
            $results[$key]['total_members'] = count($value->members->toArray());
        }

        return [
            'data' => $results,
            'meta' => [
                'count' => count($results),
                'current_page' => 1,
                'links' => array(),
                'per_page' => 10,
                'total' => count($results)
            ]
        ];
    }

}