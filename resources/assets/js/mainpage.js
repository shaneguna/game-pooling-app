/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import httpPlugin from './plugins/http';
import VueRouter from 'vue-router';
import store from './vuex/mainpage/store.js';
import routes from './routes/mainpage-routes.js';
import Slick from 'vue-slick';
import DaySpanVuetify from 'dayspan-vuetify'
import 'dayspan-vuetify/dist/lib/dayspan-vuetify.min.css'

import axios from 'axios';
import API from './API';
import App from './App.vue';

import Vuetify from 'vuetify'

import VueGeolocation from 'vue-browser-geolocation';/*
import * as VueGoogleMaps from 'vue2-google-maps'*/

import vueEventCalendar from 'vue-event-calendar'
import VeeValidate from 'vee-validate'
import { Validator } from 'vee-validate';
import VuetifyGoogleAutocomplete from 'vuetify-google-autocomplete';

window.Vue = require('vue');
window.toastr = require('toastr/build/toastr.min.js');

import('../../../node_modules/vuetify/dist/vuetify.min.css');
import('../../../node_modules/font-awesome/css/font-awesome.css');
import('../../../node_modules/slick-carousel/slick/slick.css');
import('../../../node_modules/material-design-icons/iconfont/material-icons.css');


Vue.use(DaySpanVuetify, {
  methods: {
    getDefaultEventColor: () => '#1976d2'
  }
});

Vue.use(vueEventCalendar, {locale: 'en', color: '#8bc34a'})

Vue.use(VueGeolocation);
Vue.use(httpPlugin);
Vue.use(VueRouter);
Vue.use(Vuetify, {
    theme: {
        primary: '#8bc34a',
        secondary: '#b0bec5',
        accent: '#8c9eff',
        error: '#b71c1c'
    }
});

Vue.use(VeeValidate);
/*Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyAbA17drd98LnPL59VIp8_z43tnXvaaMTI',
        libraries: 'places', // This is required if you use the Autocomplete plugin
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)
    }
})*/

Vue.use(VuetifyGoogleAutocomplete, {
    apiKey: 'AIzaSyAbA17drd98LnPL59VIp8_z43tnXvaaMTI', // Can also be an object. E.g, for Google Maps Premium API, pass `{ client: <YOUR-CLIENT-ID> }`
    /*version: '...',*/ // Optional
});

Validator.extend('isBigger', (max_age, [min_age]) => {
    return max_age >= min_age;
});



Validator.extend('isSmaller', (min_age, [max_age]) => {
    return min_age <= max_age;
});


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component(
    'vue-table',
    require('@components/VueTable.vue')
);

Vue.component('modal', {
    template: '#modal-template'
})

Vue.component('slick', Slick);

Vue.component(
    'snack-bar',
    require('@components/SnackBar.vue')
);

const router = new VueRouter({
    routes,
    mode: 'history',
    base: __dirname,
    linkExactActiveClass: 'active'
});

window.axios = axios
window.API = API
window.router = router

const app = new Vue({
    router,
    store,
    el: '#app',
    render: h => h(App)
});
