import * as types from './mutation-types'

export const mutations = {
    [types.TOGGLE_SIDEBAR](state) {
        state.sidebar.open = !state.sidebar.open
    },
    [types.UPDATE_AUTH](state, payload) {
            state.tokens = payload.tokens
            state.auth.admin_authenticated = true
            state.currentUser = payload.user
    }
}



