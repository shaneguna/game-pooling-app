import Vue from 'vue';
import Vuex from 'vuex';
import {actions} from './actions';
import {mutations} from './mutations';
import {getters} from './getters';
import {plugins} from './plugins';

Vue.use(Vuex);

const state = {
    sidebar: {
        open: false
    },
    tokens: {
        access_token: null,
        expires_in: null,
        refresh_token: null,
        token_type: null,
    },
    currentUser: {
        name: null,
        email: null,
        role: null,
    },
    auth: {
        admin_authenticated: null
    }
};

export default new Vuex.Store({
    plugins,
    state,
    actions,
    mutations,
    getters
});