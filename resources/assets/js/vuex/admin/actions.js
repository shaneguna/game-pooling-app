export const actions = {
    toggleSidebar: ({commit}) => commit('TOGGLE_SIDEBAR'),
    // facebook_login(context, access_token) {
    //     return new Promise((resolve, reject) => {

    //         let data = {
    //             access_token: access_token
    //         }

    //         axios.post('/api/auth/login/facebook', data)
    //             .then(response => {
    //                 let responseData = response.data
    //                 responseData.fb_authentication = true;

    //                 context.commit('updateTokens', responseData)

    //                 resolve(response)
    //             })
    //             .catch(response => {
    //                 reject(response)
    //             })
    //     })
    // },
    signup(context, user) {
        let data = {
          email:user.email,
          player_nickname:user.nickname,
          firstname:user.firstname,
          lastname:user.lastname,
          password:user.password,
          type: 'player',
          login_type:'passport',
        };
        console.log(data)

        axios.post('/api/player', data)
            .then(response => {
                console.log('Created player');
                window.location.href = "/"
            })
            .catch(response => {
                console.log(response)
           })
    },
    login(context, user) {
        return new Promise((resolve, reject) => {

            let data = {
                username: user.email,
                password: user.password,
                type    : 'admin'
            };

            axios.post('/api/auth/login', data)
                .then(response => {
                    // console.log(response)
                    let responseData = response.data
                    if(responseData.code !== '405'){                       
                        responseData.fb_authentication = false
                        context.commit('UPDATE_AUTH', responseData)
                        resolve(response)
                    }
                    else{
                        console.log("Error Credentials");
                    }

                })
                .catch(response => {
                    reject(response)
                })
        })
    },
    authenticate(context) {

        let payload = {
            currentUser: localStorage.getItem('currentUser'),
            tokens: localStorage.getItem('tokens')
        };
        let result = false;

        if (currentUser && tokens) {
            context.commit('LOGIN_SUCCESS', payload);
            result = true;
        }

        return result;
    }
}