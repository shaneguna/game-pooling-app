export const getters = {
    getSidebarToggleState: (state) => state.sidebar.open,
    getLoggedInStatus: (state) => state.admin_authenticated,
    getAccessToken: (state) => state.tokens.access_token,
}