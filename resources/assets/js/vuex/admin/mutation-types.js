export const TOGGLE_SIDEBAR = 'TOGGLE_SIDEBAR'

export const ACCESS_TOKEN = 'ACCESS_TOKEN'

export const LOGIN_SUCCESS = "LOGIN_SUCCESS"

export const UPDATE_AUTH = "UPDATE_AUTH"