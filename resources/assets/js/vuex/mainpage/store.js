import Vue from 'vue';
import Vuex from 'vuex';
import {actions} from './actions';
import {mutations} from './mutations';
import {getters} from './getters';
import {plugins} from './plugins';

Vue.use(Vuex);

const state = {
    user_authenticated: {
        name: "Test User",
        email: "test@gmail.com",
        profile_picture: "https://avatars0.githubusercontent.com/u/9064066?v=4&s=460",
        gender: null,
        profile_letter: null
    },
    tokens: {
        access_token: null,
        refresh_token: null,
        bearer_type: null,
        expires_in: null
    },
    auth: {
        fb_authenticated: false,
        authenticated: false,
    },
    last_route : '/',
    conversation: [],
    teams: [],
    team: [],
    categories: {},
    games: [],
    loader: false,
    new_messages: [],
    notifications: [],
    team_notifications :[],
    messages: [],
    message_dialog: false,
    message_receiver: null,
    sidebar: {
        drawer: true,
        mini: true,
        items: [
            {icon: 'dashboard', text: 'Home', link: '/'},
            {icon: 'group', text: 'My Team', link: '/teams'},
            {icon: 'fa fa-gamepad', text: 'Games', link: '/games'},
            {icon: 'fa fa-gamepad', text: 'Game Calendar', link: '/calendar'},
            {icon: 'account_circle', text: 'Matches', link: '/matches'},
            {icon: 'gavel', text: 'Terms And Agreement', link: '/terms-and-agreement'},
            {icon: 'info', text: 'Contact Us', link: '/contact'},
            {icon: 'fa fa-trophy', text: 'Tournaments', link: '/tournaments'},

        ]
    },
    match: {
        matchDetails: {
            matchMode: 'Solo'
        },
        matchParticipants : {
            ally: {},
            foe: {}
        }
    },
    current_tournament: {
        contestant_number: 0,
        created_at:null,
        start_date:null,
        team_description:null,
        tournament_id:null,
        tournament_name:'',
        type:null
    }
};

export default new Vuex.Store({
    plugins,
    state,
    actions,
    mutations,
    getters
});