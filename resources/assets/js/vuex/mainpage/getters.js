export const getters = {
    getSidebarToggleState: (state) => state.sidebar,
    getNotificationCount : (state) => {
       return state.notifications.filter(notification => notification.shown === false)
    },
    getNewMessages: (state) =>{
        return state.new_messages;
    },
    getNewTeamMessageCount: (state) =>{
        return state.team_notifications;
    },
    getNewMessageCount : (state) => {
        if(state.new_messages !== undefined)
        {
            return state.new_messages.filter(new_message => new_message.shown === false)
        }
    },
    getTeamNotifCount : (state) => {
        if(state.team_notifications !== undefined)
        {
            return state.team_notifications.filter(team_notif => team_notif.shown === false)
        }
    },
    getMessageCount : (state) => {
       return state.messages.filter(message => message.shown === false)
    },
    getCategoryWithGames: (state) => (category_name) => {
       return state.categories.find(category => category.category_name === category_name)
    },
    getMessages: (state) => {
        return state.messages
    },
    getWritingConversation: (state) => {
        return state.convesation
    },
    getTeams: (state) => state.teams,
    getMessageDialog: (state) => state.message_dialog,
    getMessageReceiver: (state) => state.message_receiver,
    getCurrentTeam: (state) => state.team,
    currentUser: (state) => state.user_authenticated,
    currentPlayer: (state) => state.user_authenticated.player,
    getCategories: (state) => state.categories,
    getGames: (state) => state.games,
    getGame: (state) => (id) => {
     return state.games.find(game => game.id === id)
    },
    getProfileLetter: state => state.user_authenticated.profile_letter,
    getMatchDetails: state => state.match.matchDetails,
    getMatchParticipantDetails: state => state.match.matchParticipants,
    getMatchMembersState: state => state.match.matchParticipants.ally,
    getMatchOpponentsState: state => state.match.matchParticipants.foe,
    getCurrentTournament: (state) => state.tournament
}