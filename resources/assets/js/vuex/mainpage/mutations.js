import * as types from './mutation-types'

export const mutations = {
    UNAUTHENTICATE_USER : (state) => {
        state.auth.authenticated = false;
        state.auth.fb_authenticated = false;

        Vue.delete( state.user_authenticated, 'player');
    },
    RECORD_LAST_ROUTE : (state, payload) => {
      state.last_route = payload;
    },
    TOGGLE_DRAWER: (state, payload) => {
        state.sidebar.drawer = payload;
    },
    TOGGLE_DRAWER_MINI_STATE: (state, payload) => {
        state.sidebar.mini = payload;
    },
    PUSH_NOTIFICATIONS: (state, payload) => {
        state.notifications.push(payload);
    },
    SET_NOTIFICATIONS: (state, payload) => {
        state.notifications = payload;
    },
    SET_NEW_MESSAGES: (state, payload) => {
        state.new_messages = payload;
    },
    SET_TEAM_MESSAGES: (state,payload) =>{
      state.team_notifications = payload;  
    },
    PUSH_MESSAGES: (state, payload) => {
        state.messages.push(payload);
    },
    SET_MESSAGES: (state,payload) => {
      state.messages = payload
    },
    SET_CONVERSATION: (state,payload) => {
      state.conversation = payload
    },
    TOGGLE_LOADER: (state, payload) => {
        state.loader = payload;
    },
    GET_CATEGORIES : (state, payload) => {
        state.categories = payload;
    },
    POPULATE_CATEGORY_WITH_GAMES : (state, payload) => {
        let category_index = state.categories.findIndex(category => category.id === payload.category_id);
        Vue.set(state.categories[category_index], 'games', payload.games.data);
        console.log("POPULATE", );
    },
    AUTHENTICATE_USER : (state, payload) => {
        state.tokens = payload.tokens
        if(payload.fb_authentication){
                state.auth.authenticated = false            
                state.auth.fb_authenticated = true
        }
        else{
            state.auth.fb_authenticated = false
            state.auth.authenticated = true            
        }
        state.user_authenticated = payload.user
    },
    FB_AUTHENTICATE: (state) => {
        state.auth.authenticated = false            
        state.auth.fb_authenticated = true
    },
    CATEGORIES: (state,payload) => {
        state.categories = payload.data
    },
    GAMES: (state,payload) => {
        state.games = payload.data
    },
    TEAMS: (state,teams) => {
        state.teams = teams
    },
    TEAM: (state,team) => {
        state.team = team
    },
    SET_MESSAGE_RECEIVER: (state, payload) => {
        state.message_receiver = payload;
    },
    PROFILE_FIRST_LETTER: (state, payload) => {
        let user = payload;
        if(user!== undefined){
            state.user_authenticated.profile_letter = user.charAt(0);
        }
        state.user_authenticated.profile_letter = ''
    },
    TOGGLE_MESSAGE_DIALOG: (state, payload) => {
        state.message_dialog = payload;
    },
    SET_MATCH_MEMBERS: (state, payload) => {
        state.match.matchParticipants.ally = payload;
    },
    SET_MATCH_OPPONENTS: (state, payload) => {
        state.match.matchParticipants.foe = payload;
    },
    SET_TOURNAMENT: (state,payload) => {
      state.tournament = payload
    }
}



