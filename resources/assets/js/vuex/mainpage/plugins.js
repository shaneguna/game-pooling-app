 // TBD if needed
import createPersistedState from 'vuex-persistedstate'

export const plugins = [createPersistedState({paths: ['tokens', 'auth', 'user_authenticated']})];
