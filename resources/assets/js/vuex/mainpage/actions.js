
export const actions = {
    toggleDrawer: ({commit}, payload) => commit('TOGGLE_DRAWER', payload),
    populateUserFirstLetter: ({commit}, payload) => commit('PROFILE_FIRST_LETTER', payload),
    toggleDrawerMiniState: ({commit}, payload) => commit('TOGGLE_DRAWER_MINI_STATE', payload),
    pushNotifications: ({commit}, payload) => commit('PUSH_NOTIFICATIONS', payload),
    setNotifications: ({commit}, payload) => commit('SET_NOTIFICATIONS', payload),
    setNewMessages: ({commit}, payload) => commit('SET_NEW_MESSAGES', payload),
    pushMessages: ({commit}, payload) => commit('PUSH_MESSAGES', payload),
    setMessages: ({commit}, payload) => commit('SET_MESSAGES', payload),
    setTeamMessages: ({commit}, payload) => commit('SET_TEAM_MESSAGES', payload),
    setConversation:({commit},payload)=> commit('SET_CONVERSATION',payload),
    toggleLoader: ({commit}, payload) => commit('TOGGLE_LOADER', payload),
    getCategories: ({commit}, payload) => {
        commit('GET_CATEGORIES',payload);
    },
    populateCategoryWithGames : ({commit}, payload) => {
        commit('POPULATE_CATEGORY_WITH_GAMES' , payload);
    },
    toggleSidebar: ({commit}) => {
        console.log('toggleSidebar');
        commit('TOGGLE_SIDEBAR')
    },
    toggleMessageDialog: ({commit},payload) => {
        commit('TOGGLE_MESSAGE_DIALOG',payload)
    },
    setMessageReceiver: ({commit},payload) => {
        commit('SET_MESSAGE_RECEIVER',payload)
    },
    login(context, user) {
        return new Promise((resolve, reject) => {

            let data = {
                username: user.email,
                password: user.password,
                type: 'player',
            };

            axios.post('/api/auth/login', data)
                .then(response => {
                    let responseData = response.data;

                    if(responseData.code !== '405'){
                        responseData.fb_authentication = false;
                        context.commit('AUTHENTICATE_USER', responseData);
                        resolve(response)
                    }
                    else{
                        reject({error : 1 , body:'Invalid Credentials'})
                    }
                })
                .catch(response => {
                    reject({error : 2 , body : response})
                })
        })
    },
    signup(context, user) {
        const self = this;
        return new Promise((resolve, reject) => {
            let data = {
              email:user.email,
              name:user.nickname,
              first_name:user.firstname,
              last_name:user.lastname,
              password:user.password,
              gender: user.gender,
              type: 'player',
              login_type:'passport',
            };

            axios.post('/api/player', data)
                .then(response => {
                    console.log('Created player', response);
                    //If status code is not equal to 201 return response data to the register.vue component
                    if(response.data.code != 201){
                        resolve(response);
                    }else{
                        axios.post('/api/auth/login', {
                            username: user.email,
                            password: user.password,
                            type: 'player',
                        })
                            .then(response => {
                                let responseData = response.data;
                                if(responseData.code !== '405'){
                                    responseData.fb_authentication = false;
                                    context.commit('AUTHENTICATE_USER', responseData);
                                    router.push('/')
                                }
                                else{
                                    router.push('/login')
                                }
                            })
                            .catch(response => {
                                //If an error occur when trying to login push user to the /login route
                                router.push('/login')
                            })

                    }

                    //If status code is 201 Log him in the application


                })
                .catch(response => {
                    reject({error : 2 , body : response})
               })
        })
    },
    facebook_login(context, access_token) {
        return new Promise((resolve, reject) => {

            let data = {
                access_token: access_token
            }

            axios.post('/api/auth/login/facebook', data)
                .then(response => {
                    let responseData = response.data
                    responseData.fb_authentication = true;

                    context.commit('AUTHENTICATE_USER', responseData)

                    resolve(response)
                })
                .catch(response => {
                    reject(response)
                })
        })
    },
    toggleFBAuthState(commit){
        context.comit('FB_AUTHENTICATE');
    },
    generateCategories (context) {
      return new Promise((resolve, reject) => {
            axios.get('/api/categories')
                .then(response => {
                    let responseData = response.data
                    console.log(response)
                    context.commit('CATEGORIES', responseData)
                    resolve(response)
                })
                .catch(response => {
                    reject(response)
                })
       })  
    },
    generateGames (context) {
      return new Promise((resolve, reject) => {
            axios.get('/api/games')
                .then(response => {
                    let responseData = response.data
                    context.commit('GAMES', responseData)
                    resolve(response)
                })
                .catch(response => {
                    reject(response)
                })
       })  
    },
    setCurrentTeam(context,team){
        context.commit('TEAM',team);
    },
    generateTeams(context,player_id){
        return new Promise((resolve, reject) => {
            let data = {
                player_id: player_id
            } 

            axios.post('/api/teams/player',data)
                .then(response => {
                    let responseData = response.data
                    context.commit('TEAMS', responseData)
                    resolve(response)
                })
                .catch(response => {
                    reject(response)
                })
        })  
    },
    setMatchMembers: ({commit}, payload) => commit('SET_MATCH_MEMBERS', payload),
    setMatchOpponents: ({commit}, payload) => commit('SET_MATCH_OPPONENTS', payload),
    setMatchMembersTempIndicator: ({commit}, payload) => commit('SET_MATCH_MEMBERS_TEMP_IND', payload),
    setMatchOpponentTempIndicator: ({commit}, payload) => commit('SET_MATCH_OPPONENT_TEMP_IND', payload),
    initiateTournament:  ({commit}, payload) => commit('SET_TOURNAMENT', payload)
}