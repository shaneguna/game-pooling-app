export default [
    {
        label: 'Dashboard',
        icon : 'ion-ios-speedometer',
        uri  : '/admin/dashboard/home'
    },
    {
        label: 'Match Claims',
        icon : 'fa fa-gamepad',
        uri  : '/admin/dashboard/matches'
    },
    {
        label: 'User',
        icon : 'fa fa-user',
        uri  : '/admin/dashboard/users'
    },
    {
        label: 'Players',
        icon : 'ion-person-stalker',
        uri  : '/admin/dashboard/players'
    },
    {
        label: 'Teams',
        icon : 'fa fa-users',
        uri  : '/admin/dashboard/teams'
    },
    {
        label: 'Category',
        icon : 'ion-ios-book',
        uri  : '/admin/dashboard/categories'
    },
    {
        label: 'Games',
        icon : 'fa fa-gamepad',
        uri  : '/admin/dashboard/games'
    },
    {
        label: 'Venues',
        icon : 'fa fa-location-arrow',
        uri  : '/admin/dashboard/venues'
    },
    {
        label: 'Tournament',
        icon : 'ion-ios-book',
        uri  : '/admin/dashboard/tournament'
    },
    {
        label: 'Announcements',
        icon : 'fa fa-info',
        uri  : '/admin/dashboard/announcements'
    },
    {
        label: 'Support',
        icon : 'fa fa-question-circle',
        uri  : '/admin/dashboard/support'
    },
    {
        label: 'System',
        icon : 'fa fa-cogs',
        uri  : '/admin/dashboard/system'
    }
]