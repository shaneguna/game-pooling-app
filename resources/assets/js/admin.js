/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import httpPlugin from './plugins/http';
import VueRouter from 'vue-router';
import 'vue-multiselect/dist/vue-multiselect.min.css';
import store from './vuex/admin/store.js';
import routes from './routes/admin-routes.js';

import axios from 'axios';
import API from './API';
import App from './App.vue';
import * as VueGoogleMaps from 'vue2-google-maps'
import wysiwyg from "vue-wysiwyg";

import datePicker from 'vue-bootstrap-datetimepicker';
import 'bootstrap/dist/css/bootstrap.css';
import 'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css';
import('../../../node_modules/font-awesome/css/font-awesome.min.css');
import BootstrapVue from 'bootstrap-vue'


window.Vue = require('vue');

window.toastr = require('toastr/build/toastr.min.js');
window.innerHeight = 800;

window.toastr.options = {
    positionClass: "toast-bottom-right",
    showDuration: "300",
    hideDuration: "1000",
    timeOut: "5000",
    extendedTimeOut: "1000",
    showEasing: "swing",
    hideEasing: "linear",
    showMethod: "fadeIn",
    hideMethod: "fadeOut"
};
Vue.use(datePicker);

Vue.use(BootstrapVue)
Vue.use(httpPlugin);
Vue.use(VueRouter);
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyAbA17drd98LnPL59VIp8_z43tnXvaaMTI',
        libraries: 'places', // This is required if you use the Autocomplete plugin
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)
    }
})
Vue.use(wysiwyg, {
    hideModules: { "image": true },
    image: {
        uploadURL: '/api/file/upload',
        dropzoneOptions: {}
    }
});
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component(
    'vue-table-pagination',
    require('@components/TablePagination.vue')
);

Vue.component(
    'vue-table',
    require('@components/VueTable.vue')
);

Vue.component(
    'vue-form',
    require('@components/Form.vue')
);

const router = new VueRouter({
    routes,
    mode: 'history',
    base: __dirname,
    linkActiveClass: 'active',
    linkExactActiveClass: 'active'
});

window.axios = axios
window.API = API

const app = new Vue({
    router,
    store,
    el: '#app',
    render: h => h(App)
});
