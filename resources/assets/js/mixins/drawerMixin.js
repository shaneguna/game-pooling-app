export default {
    computed: {
        isUserAuthenticated(){
            const self = this;
            if(self.$store.state.auth.fb_authenticated || self.$store.state.auth.authenticated){
                return true;
            }else{
                return false;
            }
        },
        drawer: {
            get() {
                return this.$store.state.sidebar.drawer;
            },
            set(value) {
                this.$store.dispatch('toggleDrawer', value);
            }
        },
        drawer_items() {
            return this.$store.state.sidebar.items;
        },
        mini: {
            get() {
                return this.$store.state.sidebar.mini;
            },
            set(value) {
                this.$store.dispatch('toggleDrawerMiniState', value);
            }
        },
        user_authenticated() {
            return this.$store.state.user_authenticated;
        }
    }
}