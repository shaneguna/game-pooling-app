export default {
    methods: {
        getCategories(){
            const self = this;
            self.$http.get('categories')
                .then((response) => {
                    self.$store.dispatch('getCategories', response.data.data);
            })
        }
    }
}