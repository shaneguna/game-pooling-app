import Dashboard from '../views/Admin/Dashboard.vue'
import Parent from '../views/Admin/Parent.vue'
import Login from '../views/Login/admin_login.vue'

export default [
    {
        path: '/admin/dashboard',
        component: Dashboard,
        beforeEnter: checkAuth,
        children: [
            {
                path: '/',
                redirect: '/admin/dashboard/home'
            },
            {
                path: 'home',
                component: () => import('../views/Admin/Home/Home.vue'),
            },
            {
                path: 'users',
                component: Parent,
                children: [
                    {
                        path: '/',
                        component: () => import('../views/Admin/User/User.vue')
                    },
                    {
                        path: 'create',
                        component: () => import('../views/Admin/User/Create.vue')
                    },
                    {
                        path: ':id/edit',
                        component: () => import('../views/Admin/User/Edit.vue')
                    }
                ]
            },
            {
                path: 'players',
                component: Parent,
                children: [
                    {
                        path: '/',
                        component: () => import('../views/Admin/Player/Player.vue')
                    },
                    {
                        path: 'create',
                        component: () => import('../views/Admin/Player/Create.vue')
                    },
                    {
                        path: ':id/edit',
                        component: () => import('../views/Admin/Player/Edit.vue')
                    }
                ]
            },
            {
                path: 'categories',
                component: Parent,
                children: [
                    {
                        path: '/',
                        component: () => import('../views/Admin/Category/Category.vue')
                    },
                    {
                        path: 'create',
                        component: () => import('../views/Admin/Category/Create.vue')
                    },
                    {
                        path: ':id/edit',
                        component: () => import('../views/Admin/Category/Edit.vue')
                    }
                ]
            },
            {
                path: 'games',
                component: Parent,
                children: [
                    {
                        path: '/',
                        component: () => import('../views/Admin/Game/Game.vue')
                    },
                    {
                        path: 'create',
                        component: () => import('../views/Admin/Game/Create.vue')
                    },
                    {
                        path: ':id/edit',
                        component: () => import('../views/Admin/Game/Edit.vue')
                    }
                ]
            },
            {
                path: 'venues',
                component: Parent,
                children: [
                    {
                        path: '/',
                        component: () => import('../views/Admin/Venue/Venue.vue')
                    },
                    {
                        path: 'create',
                        component: () => import('../views/Admin/Venue/Create.vue')
                    },
                    {
                        path: ':id/edit',
                        component: () => import('../views/Admin/Venue/Edit.vue')
                    }
                ]
            },
            {
                path: 'announcements',
                component: Parent,
                children: [
                    {
                        path: '/',
                        component: () => import('../views/Admin/Announcement/Announcement.vue')
                    },
                    {
                        path: 'create',
                        component: () => import('../views/Admin/Announcement/Create.vue')
                    },
                    {
                        path: ':id/edit',
                        component: () => import('../views/Admin/Announcement/Edit.vue')
                    }
                ]
            },
            {
                path: 'tournament',
                component: Parent,
                children: [
                    {
                        path: '/',
                        component: () => import('../views/Admin/Tournament/Tournament.vue')
                    },
                    {
                        path: 'create',
                        component: () => import('../views/Admin/Tournament/Create.vue')
                    },
                    {
                        path: ':id/edit',
                        component: () => import('../views/Admin/Tournament/Edit.vue')
                    },
                    {
                        name: 'TournamentEdit',
                        path: ':id/edit/details',
                        props: true,
                        component: () => import('../views/Admin/Tournament/EditForm.vue')
                    }                    
                ]
            },
            {
                path: 'matches',
                component: Parent,
                children: [
                    {
                        path: '/',
                        component: () => import('../views/Admin/MatchClaims/Index.vue')
                    },
                    {
                        path: ':id/edit',
                        component: () => import('../views/Admin/MatchClaims/Edit.vue')
                    }
                ]
            },
            {
                path: 'teams',
                component: Parent,
                children: [
                    {
                        path: '/',
                        component: () => import('../views/Admin/Team/Team.vue')
                    },
                    // {
                    //     path: 'create',
                    //     component: () => import('../views/Admin/Tournament/Create.vue')
                    // },
                    {
                        path: ':id/edit',
                        component: () => import('../views/Admin/Team/Edit.vue')
                    }
                ]
            },

            {
                path: 'system',
                component: () => import('../views/Admin/System/System.vue')
            },
            {
                path: 'support',
                component: () => import('../views/Admin/Support/Terms.vue')
            },
            {
                path: '*',
                redirect: '/admin/dashboard/home'
            }
        ]
    },
    {
        path: '/admin/login',
        component: Login,
    }
]

function checkAuth(to, from, next) {

    let admin_authenticated = (JSON.parse(localStorage.getItem('vuex'))) ? JSON.parse(localStorage.getItem('vuex')).auth.admin_authenticated : false;
    
    if (admin_authenticated) next()

    else {
        window.location.href = '/admin/login';
    }
}

