import Login from '../views/Login/login.vue'
import HomeDashboard from '../views/MainPage/Home/Dashboard.vue'
import store from '../vuex/mainpage/store.js';

export default [
    {
        path: '/',
        component: HomeDashboard,
        redirect: '/',
        children: [
            {
                path: '',
                component: () => import('../views/MainPage/Home/Home.vue')
            },
            {
                path: 'directory',
                component: () => import('../views/MainPage/Directory/Directory.vue')
            },
            {
                path: 'matches',
                component: () => import('../views/MainPage/Matches/MatchesPage.vue')
            },
            {
                path: '/directory/:name',
                component: () => import('../views/MainPage/Directory/subroutes/Category.vue'),
                props: true
            },
            {
                path: '/directory/:category/:gameID/:tab?',
                component: () => import('../views/MainPage/Directory/subroutes/Game.vue'),
                props: true,
                canReuse: false
            },
            {
                path: '/directory/:name/:id/teams/:teamname',
                component: () => import('../views/MainPage/Directory/subroutes/Game.vue'),
                props: true,
                canReuse: false
            },
            {
                path: '/profile/:id',
                component: () => import('../views/MainPage/Profile/Profile.vue'),
                props: true,
                canReuse: false
            },
            {
                path: '/match/solo/:match_id',
                component: () => import('../views/MainPage/Matches/MatchPage.vue'),
                props: true,
                canReuse: false
            },
            {
                name: 'MatchPage',
                path: '/match/team/:match_id',
                component: () => import('../views/MainPage/Matches/MatchPage.vue'),
                props: true,
                canReuse: false
            },
            {
                path: '/404',
                component: () => import('../views/MainPage/ErrorPage/404.vue'),
            },
            {
                path: '/teams',
                beforeEnter: authCheck,
                component: () => import('../views/MainPage/Team/Team.vue'),
            },
            {
                path: '/message',
                beforeEnter: authCheck,
                component: () => import('../views/MainPage/Chat/DirectMessage.vue'),
            },
            {
                path: '/teams/:id',
                component: () => import('../views/MainPage/Team/TeamView.vue'),
                props: true,
                canReuse: false
            },
            {
                path: '/calendar',
                component: () => import('../views/MainPage/GameCalendar/EventCalendar.vue'),
                props: true,
                beforeEnter: authCheck,
                canReuse: false
            },
            {
                path: '/terms-and-agreement',
                component: () => import('../views/MainPage/Support/TermsAndAgreement.vue'),
                props: true,
                canReuse: false
            },
            {
                path: '/games',
                component: () => import('../views/MainPage/Games/Games.vue'),
                props: true,
                beforeEnter: authCheck,
                canReuse: false
            },
            {
                path: '/contact',
                component: () => import('../views/MainPage/Support/ContactDetails.vue'),
                props: true,
                canReuse: false
            },
            {
                path: '/announcement/:name',
                component: () => import('../views/MainPage/Announcement/ViewAnnouncement.vue'),
                props: true,
                canReuse: false
            },
            {
                path: '/tournaments',
                component: () => import('../views/MainPage/Tournament/TournamentsPage.vue'),
                props: true,
                canReuse: false
            },
            {
                path: 'tournament/:id/tournament-details/',
                component: () => import('../views/MainPage/Tournament/Tournament.vue'),
                props: true,
                canReuse: false
            }

        ]
    },
    {
        path: '/login',
        component: Login,
        beforeEnter: authCheck,
    },
    {
        path: '/register',
        component: () => import('@views/Register/Register.vue')
    }
]

function authCheck(from, to, next) {
    let fb_authenticated = (JSON.parse(localStorage.getItem('vuex'))) ? JSON.parse(localStorage.getItem('vuex')).auth.fb_authenticated : false;
    let authenticated = (JSON.parse(localStorage.getItem('vuex'))) ? JSON.parse(localStorage.getItem('vuex')).auth.authenticated : false;
    let fb_redirected = window.localStorage.getItem('fb_redirected');

    if (authenticated || fb_authenticated) {
        /*  If user is authenticated and trying to access the /login route
            re-direct him to / route
            else redirect him to the route the user is trying to access
        */
        (from.fullPath == '/login')
            ? next({ path: '/' })
            : next()
    } else if (fb_redirected && !fb_authenticated) {
        window.fbAsyncInit = function () {
            FB.init({
                appId: '227714717768787',
                xfbml: true,
                version: 'v2.11'
            });
            FB.getLoginStatus(function (response) {
                if (response.status === 'connected') {
                    var accessToken = response.authResponse.accessToken;
                    store.dispatch('facebook_login', accessToken)
                        .then(response => {
                            window.location.href = "/";
                        })
                        .catch(response => {
                            let errorMessage = response.response.data.message
                            alert(errorMessage)
                        })
                }
                else {
                    localStorage.setItem('fb_redirected', 'true');
                    window.location.href = "/api/auth/facebook";
                }
            });
        };
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    } else {
        /*  If user is not authenticated check if user is trying to access /login route
            if yes let him access /login
            if not, Restrict him for the route he is trying to access by redirecting him to / route
        */
        (from.fullPath == '/login')
            ? next()
            : next({ path: '/' });

    }
}


