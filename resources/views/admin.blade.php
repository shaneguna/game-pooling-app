<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Game Pooling App</title>
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">

    <link href="{{ mix('css/admin.css') }}" rel="stylesheet" type="text/css">

    <style>
        [v-cloak] {
            display: none;
        }
    </style>

    <script>
        window.Laravel = {
            csrfToken: "{{ csrf_token() }}"
        }
        window.User = "{!! Auth::user() !!}"
    </script>
</head>
<body>
<div id="app"></div>
<script src="{{ mix('js/admin.js') }}"></script>
</body>
</html>