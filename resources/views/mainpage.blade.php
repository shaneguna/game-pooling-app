<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Game Pooling App</title>
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">

    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>

    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,700|Material+Icons' rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Gudea" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    {{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAbA17drd98LnPL59VIp8_z43tnXvaaMTI&libraries=places"></script>--}}

    <link href="{{ mix('css/mainpage.css') }}" rel="stylesheet" type="text/css">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">


    <style>
        [v-cloak] {
            display: none;
        }
    </style>

    <script>
        window.Laravel = {
            csrfToken: "{{ csrf_token() }}"
        }
        window.User = "{!! Auth::user() !!}"
    </script>
</head>
<body>
<div id="app"></div>
<script src="{{ mix('js/mainpage.js') }}"></script>
</body>
</html>