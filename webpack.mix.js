let mix = require('laravel-mix');
const path = require('path');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

function excludeNodeModulesExcept (modules)
{
    let pathSep = path.sep;
    if (pathSep == '\\') // must be quoted for use in a regexp:
        pathSep = '\\\\';
    let moduleRegExps = modules.map (function (modName) { return new RegExp("node_modules" + pathSep + modName)})

    return function (modulePath) {
        if (/node_modules/.test(modulePath)) {
            for (let i = 0; i < moduleRegExps.length; i ++)
                if (moduleRegExps[i].test(modulePath)) return false;
            return true;
        }
        return false;
    };
}

mix.webpackConfig({
    output: {
        publicPath: "/",
        chunkFilename: 'js/chunks/[name].[chunkhash].js'
    },
    resolve: {
        alias: {
            '@mixins': path.resolve(__dirname, 'resources/assets/js/mixins'),
            '@components': path.resolve(__dirname, 'resources/assets/js/components'),
            '@config': path.resolve(__dirname, 'resources/assets/js/config'),
            '@plugins': path.resolve(__dirname, 'resources/assets/js/plugins'),
            '@views': path.resolve(__dirname, 'resources/assets/js/views'),
            '@admin': path.resolve(__dirname, 'resources/assets/js/views/admin'),
            '@mainpage': path.resolve(__dirname, 'resources/assets/js/views/MainPage')
        },
        modules: [
            'node_modules',
            path.resolve(__dirname, "resources")
        ]
    },
    module: {
        loaders: [{
            test: /\.vue$/,
            loader: 'vue-loader',
            options: { presets: ['es2015' ] },
            exclude: excludeNodeModulesExcept(["vuetify-google-autocomplete"])
        }]
    },
})

mix.js('resources/assets/js/admin.js', 'public/js')
    .js('resources/assets/js/mainpage.js', 'public/js')
    .sass('resources/assets/sass/admin.scss', 'public/css')
    .sass('resources/assets/sass/mainpage.scss', 'public/css')
/*.version()*/;
