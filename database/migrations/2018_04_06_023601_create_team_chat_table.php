<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeamChatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('team_chat', function(Blueprint $table)
		{
			$table->increments('team_chat_id');
			$table->integer('team_id')->unsigned()->index('team_chat_team_id_foreign');
			$table->integer('player_id')->unsigned()->index('team_chat_player_id_foreign');
			$table->string('message')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('team_chat');
	}

}
