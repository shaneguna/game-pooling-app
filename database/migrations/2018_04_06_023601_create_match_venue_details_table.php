<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMatchVenueDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('match_venue_details', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('match_id')->unsigned()->index('venue_match_id_foreign');
			$table->string('venue_name')->nullable();
			$table->string('long')->nullable();
			$table->string('lat')->nullable();
			$table->string('vicinity');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('match_venue_details');
	}

}
