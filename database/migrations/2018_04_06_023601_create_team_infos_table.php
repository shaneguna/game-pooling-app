<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeamInfosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('team_infos', function(Blueprint $table)
		{
			$table->increments('team_id');
			$table->integer('category_id')->unsigned()->index('team_infos_category_id_foreign');
			$table->integer('game_id');
			$table->string('team_name');
			$table->string('status');
			$table->integer('team_win');
			$table->integer('team_loss');
			$table->integer('team_rating');
			$table->string('team_description');
			$table->integer('is_temporary')->nullable()->default(0);
			$table->timestamps();
			$table->integer('team_leader')->unsigned()->index('team_infos_team_leader_foreign');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('team_infos');
	}

}
