<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMatchInvites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_invites', function (Blueprint $table) {
            $table->increments('match_invite_id');
            $table->integer('match_id')->unsigned()->index('match_invites_match_id_foreign');
            $table->integer('team_id')->default(0);
            $table->integer('player_id')->default(0);
            $table->enum('invite_mode',['request_to_join','invited']);
            $table->enum('type', ['Pending', 'Approved', 'Reject'])->default('Pending');
            $table->timestamps();
            $table->foreign('match_id')->references('match_id')->on('matches')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('match_invites', function (Blueprint $table) {
            $table->dropForeign('match_invites_match_id_foreign');
        });

        Schema::dropIfExists('match_invites');
    }
}
