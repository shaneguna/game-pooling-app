<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFriendshipsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('friendships', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('sender_id')->unsigned();
			$table->string('sender_type');
			$table->integer('recipient_id')->unsigned();
			$table->string('recipient_type');
			$table->boolean('status')->default(0);
			$table->timestamps();
			$table->index(['sender_id','sender_type']);
			$table->index(['recipient_id','recipient_type']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('friendships');
	}

}
