<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlayerMatchChatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('player_match_chat', function(Blueprint $table)
		{
			$table->foreign('match_id')->references('match_id')->on('matches')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('player_receiver')->references('player_id')->on('players')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('player_sender')->references('player_id')->on('players')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('player_match_chat', function(Blueprint $table)
		{
			$table->dropForeign('player_match_chat_match_id_foreign');
			$table->dropForeign('player_match_chat_player_receiver_foreign');
			$table->dropForeign('player_match_chat_player_sender_foreign');
		});
	}

}
