<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableMatchRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_requests', function (Blueprint $table) {
            $table->increments('match_request_id');
            $table->integer('match_id')->unsigned()->index('match_requests_match_id_foreign');
            $table->integer('team_id')->unsigned()->index('match_requests_team_id_foreign');
            $table->enum('type', ['Pending', 'Approved', 'Reject'])->default('Pending');
            $table->timestamps();

            $table->foreign('team_id')->references('team_id')->on('team_infos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('match_id')->references('match_id')->on('matches')->onUpdate('RESTRICT')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('match_requests', function (Blueprint $table) {
            $table->dropForeign('match_requests_match_id_foreign');
            $table->dropForeign('match_requests_team_id_foreign');
        });

        Schema::dropIfExists('match_requests');
    }
}
