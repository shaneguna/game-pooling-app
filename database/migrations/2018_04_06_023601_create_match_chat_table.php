<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMatchChatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('match_chat', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('match_id')->unsigned()->index('match_chat_match_id_foreign');
			$table->integer('player_id')->unsigned()->index('match_chat_player_id_foreign');
			$table->string('message');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('match_chat');
	}

}
