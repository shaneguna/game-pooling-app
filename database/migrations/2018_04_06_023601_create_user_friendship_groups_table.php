<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserFriendshipGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_friendship_groups', function(Blueprint $table)
		{
			$table->integer('friendship_id')->unsigned();
			$table->integer('friend_id')->unsigned();
			$table->string('friend_type');
			$table->integer('group_id')->unsigned();
			$table->unique(['friendship_id','friend_id','friend_type','group_id'], 'unique');
			$table->index(['friend_id','friend_type']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_friendship_groups');
	}

}
