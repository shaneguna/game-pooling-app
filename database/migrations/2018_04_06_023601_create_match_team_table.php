<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMatchTeamTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('match_team', function(Blueprint $table)
		{
			$table->integer('team_id')->unsigned()->index('match_team_team_id_foreign');
			$table->integer('match_id')->unsigned();
			$table->primary(['match_id','team_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('match_team');
	}

}
