<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVenueGameTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('venue_game', function(Blueprint $table)
		{
			$table->foreign('game_id')->references('game_id')->on('games')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('venue_id')->references('venue_id')->on('venues')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('venue_game', function(Blueprint $table)
		{
			$table->dropForeign('venue_game_game_id_foreign');
			$table->dropForeign('venue_game_venue_id_foreign');
		});
	}

}
