<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('players', function(Blueprint $table)
		{
			$table->increments('player_id');
			$table->integer('user_id')->unsigned()->index('players_user_id_foreign');
			$table->string('player_nickname')->nullable();
			$table->string('profile_picture')->nullable();
			$table->integer('player_rating')->nullable();
			$table->integer('player_win')->nullable();
			$table->integer('player_loss')->nullable();
			$table->string('player_details')->nullable();
			$table->boolean('public_profile')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('players');
	}

}
