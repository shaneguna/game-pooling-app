<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournaments', function (Blueprint $table) {
            $table->increments('tournament_id');
            $table->integer('category_id')->unsigned()->index('tournaments_category_id_foreign');
            $table->integer('game_id')->unsigned()->index('tournaments_game_id_foreign');
            $table->string('tournament_name');
            $table->string('details')->default('');
            $table->string('tournament_description');
            $table->string('winner')->default('');
            $table->enum('type',['Team','Individual']);
            $table->timestamp('start_date');
            $table->timestamps();
        
            $table->foreign('category_id')->references('category_id')->on('categories')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('game_id')->references('game_id')->on('games')->onUpdate('RESTRICT')->onDelete('RESTRICT');

        });

        Schema::create('tournament_contestants', function (Blueprint $table){
            $table->increments('id');
            $table->integer('tournament_id')->unsigned()->index('tournament_contestants_tournament_id_foreign');
            $table->integer('team_id')->unsigned()->index('tournament_contestants_team_id_foreign');
            $table->integer('player_id')->unsigned()->index('tournament_contestants_player_id_foreign');
            $table->timestamps();

            $table->foreign('tournament_id')->references('tournament_id')->on('tournaments')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('team_id')->references('team_id')->on('team_infos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('player_id')->references('player_id')->on('players')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });

        Schema::create('tournament_applicants', function (Blueprint $table){
            $table->integer('tournament_id')->unsigned()->index('tournament_applicants_tournament_id_foreign');
            $table->integer('team_id')->unsigned()->index('tournament_applicants_team_id_foreign');
            $table->integer('player_id')->unsigned()->index('tournament_applicants_player_id_foreign');
            $table->timestamps();

            $table->foreign('tournament_id')->references('tournament_id')->on('tournaments')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('team_id')->references('team_id')->on('team_infos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('player_id')->references('player_id')->on('players')->onUpdate('RESTRICT')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('tournaments', function(Blueprint $table)
        {
            $table->dropForeign('tournaments_category_id_foreign');
            $table->dropForeign('tournaments_game_id_foreign');
        });

        Schema::table('tournament_contestants', function(Blueprint $table)
        {
            $table->dropForeign('tournament_contestants_tournament_id_foreign');
            $table->dropForeign('tournament_contestants_team_id_foreign');
            $table->dropForeign('tournament_contestants_player_id_foreign');
        });
        
        Schema::table('tournament_applicants', function(Blueprint $table)
        {
            $table->dropForeign('tournament_applicants_tournament_id_foreign');
            $table->dropForeign('tournament_applicants_team_id_foreign');
            $table->dropForeign('tournament_applicants_player_id_foreign');
        });

        Schema::dropIfExists('tournaments');
        Schema::dropIfExists('tournament_applicants');
        Schema::dropIfExists('tournament_contestants');
    }
}
