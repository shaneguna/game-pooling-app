<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVenuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('venues', function(Blueprint $table)
		{
			$table->increments('venue_id');
			$table->string('venue_name');
			$table->string('venue_description');
			$table->string('venue_image');
			$table->integer('long');
			$table->integer('lat');
			$table->integer('capacity');
			$table->string('address');
			$table->string('city');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('venues');
	}

}
