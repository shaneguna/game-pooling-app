<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserFriendshipGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_friendship_groups', function(Blueprint $table)
		{
			$table->foreign('friendship_id')->references('id')->on('friendships')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_friendship_groups', function(Blueprint $table)
		{
			$table->dropForeign('user_friendship_groups_friendship_id_foreign');
		});
	}

}
