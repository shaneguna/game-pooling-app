<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchConclusionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_conclusions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('match_id')->unsigned()->index('match_conclusions_match_id_foreign');
            $table->integer('admin_user_id')->unsigned()->index('match_conclusions_user_id_foreign')->nullable();
            $table->string('admin_notes')->nullable();
            $table->enum('creator_claim',['Win','Lose','Draw'])->nullable();
            $table->string('creator_statement')->nullable();
            $table->string('creator_image_attachment')->nullable();
            $table->string('creator_document_attachment')->nullable();
            $table->enum('opponent_claim',['Win','Lose','Draw'])->nullable();
            $table->string('opponent_statement')->nullable();
            $table->string('opponent_image_attachment')->nullable();
            $table->string('opponent_document_attachment')->nullable();
            $table->enum('judgement_to_creator',['Win','Lose','Draw'])->nullable();
            $table->enum('judgement_to_opponent',['Win','Lose','Draw'])->nullable();
            $table->timestamps();

            $table->foreign('match_id')
                ->references('match_id')
                ->on('matches')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');

            $table->foreign('admin_user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('match_conclusions', function (Blueprint $table) {
            $table->dropForeign('match_conclusions_match_id_foreign');
            $table->dropForeign('match_conclusions_user_id_foreign');
        });

        Schema::dropIfExists('match_conclusions');
    }
}
