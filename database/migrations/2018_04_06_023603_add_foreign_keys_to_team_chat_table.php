<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTeamChatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('team_chat', function(Blueprint $table)
		{
			$table->foreign('player_id')->references('player_id')->on('players')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('team_id')->references('team_id')->on('team_infos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('team_chat', function(Blueprint $table)
		{
			$table->dropForeign('team_chat_player_id_foreign');
			$table->dropForeign('team_chat_team_id_foreign');
		});
	}

}
