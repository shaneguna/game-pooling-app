<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeamInvitesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('team_invites', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('team_id')->unsigned()->index('team_invites_team_id_foreign');
			$table->integer('player_id')->unsigned()->index('team_invites_player_id_foreign');
			$table->enum('status', array('Pending','Accepted','Rejected'));
			$table->enum('mode', array('player invite','team invite'));
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('team_invites');
	}

}
