<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayerMatchChatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('player_match_chat', function(Blueprint $table)
		{
			$table->increments('player_match_chat_id');
			$table->integer('match_id')->unsigned()->index('player_match_chat_match_id_foreign');
			$table->integer('player_receiver')->unsigned()->index('player_match_chat_player_receiver_foreign');
			$table->integer('player_sender')->unsigned()->index('player_match_chat_player_sender_foreign');
			$table->string('message')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_match_chat');
	}

}
