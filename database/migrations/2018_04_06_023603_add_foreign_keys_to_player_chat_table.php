<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlayerChatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('player_chat', function(Blueprint $table)
		{
			$table->foreign('player_receiver')->references('player_id')->on('players')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('player_sender')->references('player_id')->on('players')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('player_chat', function(Blueprint $table)
		{
			$table->dropForeign('player_chat_player_receiver_foreign');
			$table->dropForeign('player_chat_player_sender_foreign');
		});
	}

}
