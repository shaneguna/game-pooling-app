<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTeamInfosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('team_infos', function(Blueprint $table)
		{
			$table->foreign('category_id')->references('category_id')->on('categories')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('team_leader')->references('player_id')->on('players')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('team_infos', function(Blueprint $table)
		{
			$table->dropForeign('team_infos_category_id_foreign');
			$table->dropForeign('team_infos_team_leader_foreign');
		});
	}

}
