<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchConfirmationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_confirmations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('match_id')->unsigned()->index('match_confirmations_match_id_foreign');
            $table->integer('team_creator_id')->default(0);
            $table->integer('player_creator_id')->default(0);
            $table->integer('team_opponent_id')->default(0);
            $table->integer('player_opponent_id')->default(0);
            $table->enum('creator_status',['confirmed','no_confirmation'])->default('no_confirmation');
            $table->enum('opponent_status',['confirmed','no_confirmation'])->default('no_confirmation');
            $table->timestamps();

            $table->foreign('match_id')->references('match_id')->on('matches')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_confirmations');
    }
}
