<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTeamMatchChatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('team_match_chat', function(Blueprint $table)
		{
			$table->foreign('match_id')->references('match_id')->on('matches')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('player_id')->references('player_id')->on('players')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('team_match_chat', function(Blueprint $table)
		{
			$table->dropForeign('team_match_chat_match_id_foreign');
			$table->dropForeign('team_match_chat_player_id_foreign');
		});
	}

}
