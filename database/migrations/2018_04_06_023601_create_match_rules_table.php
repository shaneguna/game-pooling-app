<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMatchRulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('match_rules', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('match_id')->nullable();
			$table->integer('min_age')->nullable();
			$table->integer('max_age')->nullable();
			$table->string('gender_rule')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('match_rules');
	}

}
