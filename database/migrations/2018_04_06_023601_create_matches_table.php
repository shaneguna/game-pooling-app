<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMatchesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('match_id');
            $table->integer('game_id')->unsigned()->index('matches_game_id_foreign');
            $table->string('match_name');
            $table->string('match_description');
            $table->enum('match_mode', array('Solo', 'Team'));
            $table->integer('match_winner')->nullable();
            $table->integer('match_loser')->nullable();
            $table->string('match_score')->nullable();
            $table->date('match_date')->nullable();
            $table->boolean('is_public')->nullable();
            $table->enum('match_status', array('Match Initiated', 'Match Over', 'Match Set', 'Match Cancelled'));
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('matches');
    }

}
