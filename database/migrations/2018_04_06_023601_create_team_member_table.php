<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeamMemberTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('team_member', function(Blueprint $table)
		{
			$table->integer('team_id')->unsigned();
			$table->integer('player_id')->unsigned()->index('team_member_player_id_foreign');
            $table->boolean('is_active')->default(true);
			$table->primary(['team_id','player_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('team_member');
	}

}
