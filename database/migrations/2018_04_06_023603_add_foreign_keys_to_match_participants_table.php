<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMatchParticipantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('match_participants', function(Blueprint $table)
		{
			$table->foreign('match_id')->references('match_id')->on('matches')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('team_id')->references('team_id')->on('team_infos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('match_participants', function(Blueprint $table)
		{
			$table->dropForeign('match_participants_match_id_foreign');
			$table->dropForeign('match_participants_team_id_foreign');
		});
	}

}
