<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGamesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('games', function(Blueprint $table)
		{
			$table->increments('game_id');
			$table->string('game_name');
			$table->string('background_image');
			$table->string('image');
			$table->integer('category_id')->unsigned()->index('games_category_id_foreign');
			$table->integer('team_member_minimum');
			$table->timestamps();
			$table->string('game_description');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('games');
	}

}
