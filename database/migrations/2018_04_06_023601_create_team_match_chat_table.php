<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeamMatchChatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('team_match_chat', function(Blueprint $table)
		{
			$table->increments('team_match_chat_id');
			$table->integer('match_id')->unsigned()->index('team_match_chat_match_id_foreign');
			$table->integer('player_id')->unsigned()->index('team_match_chat_player_id_foreign');
			$table->string('message')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('team_match_chat');
	}

}
