<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVenueGameTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('venue_game', function(Blueprint $table)
		{
			$table->integer('venue_id')->unsigned();
			$table->integer('game_id')->unsigned()->index('venue_game_game_id_foreign');
			$table->primary(['venue_id','game_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('venue_game');
	}

}
