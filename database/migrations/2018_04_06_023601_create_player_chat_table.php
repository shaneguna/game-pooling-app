<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayerChatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('player_chat', function(Blueprint $table)
		{
			$table->increments('player_chat_id');
			$table->integer('player_receiver')->unsigned()->index('player_chat_player_receiver_foreign');
			$table->integer('player_sender')->unsigned()->index('player_chat_player_sender_foreign');
			$table->string('message')->nullable();
			$table->timestamps();
			$table->boolean('seen')->default(0);
			$table->string('channel');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_chat');
	}

}
