<?php

Route::group(['namespace' => 'Category'], function () {
    Route::resource('category', 'CategoryController', ['except' => ['create', 'show']]);
    Route::get('/categories', 'CategoryController@getList');
    Route::get('/category/{name}/name', 'CategoryController@getCategoryByName');
    Route::post('/category/{id}/status', 'CategoryController@status');
    Route::post('/categories/byPage', 'CategoryController@byPage');

});


