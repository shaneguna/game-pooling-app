<?php

Route::group(['namespace' => 'Search'], function () {
    Route::resource('search', 'SearchController', ['except' => ['create','show']]);
    Route::get('/search/{id}', 'SearchController@getSearchedResult');
});


