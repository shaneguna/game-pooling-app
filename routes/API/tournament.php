<?php

Route::group(['namespace' => 'Tournament'], function () {
    Route::resource('tournament', 'TournamentController', ['except' => ['create']]);
    Route::get('/tournament/{id}/contestant','TournamentController@getListOfContestant');
    Route::put('/tournament/{tournament}/update-contestant','TournamentController@updateTournamentContestant');
    Route::put('/tournament/{tournament}/update-image','TournamentController@updateImage');
    Route::post('/tournament/add-contestant','TournamentController@addContestant');
    Route::put('/tournament/{tournament}/edit','TournamentController@update');
	}
);


