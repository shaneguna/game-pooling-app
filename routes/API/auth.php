<?php 

Route::group(['namespace' => 'Auth'], function(){


	// api login
	Route::post('/auth/login','AdminLoginController@login');

	// socialite login
	Route::get('auth/{provider}', 'SocialiteLoginController@redirectToProvider');
	Route::get('/facebook/callback', 'SocialiteLoginController@handleProviderCallback');	
	Route::post('auth/login/facebook', 'SocialiteLoginController@loginFacebook');	
});



