<?php

Route::group(['namespace' => 'Venue'], function () {
    Route::resource('venue', 'VenueController', ['except' => ['create', 'show']]);
    Route::get('/venues', 'VenueController@getList');
    Route::post('/venue/{id}/status', 'VenueController@status');
});


