<?php

Route::group([
    'namespace' => 'Upload',
], function () {
    // File Upload
    //Route::post('file/upload', 'UploadController@fileUpload')->middleware('auth:api');
    Route::post('file/upload', 'UploadController@fileUpload');
    Route::post('file/delete', 'UploadController@deleteFile');
});