<?php
Route::get('system', 'SystemController@getSystemInfo');
Route::post('update-support-details', 'SystemController@updateSupportDetails');
Route::get('get-support-details', 'SystemController@getSupportDetails');