<?php

Route::group(['namespace' => 'Team'], function () {
    Route::resource('team', 'TeamController', ['except' => ['create', 'show']]);
    Route::get('/teams', 'TeamController@getList');
    Route::get('/get-team-members/{id}', 'TeamController@getTeamMembers');
    Route::post('/teams/player', 'TeamController@getTeamsByPlayer');
    Route::post('/team/{id}/status', 'TeamController@status');
    Route::post('/team/{id}/requests', 'TeamInviteController@invitationList');
    Route::post('/team/invite/player', 'TeamInviteController@teamInvitation');
    Route::post('/team/join', 'TeamInviteController@joinTeam');
    Route::post('/team/invite/accept', 'TeamInviteController@acceptInvite');
    Route::post('/team/invite/reject', 'TeamInviteController@rejectInvite');
    Route::get('/team/{id}/matches', 'TeamController@generateTeamMatches');

    Route::post('/team/message', 'TeamChatController@sendMessage');
    Route::get('/team/chat', 'TeamChatController@teamMessages');
    Route::post('/team/unread-chat', 'TeamChatController@unreadTeamMessages');
    Route::post('/team/read-chat-notif', 'TeamChatController@updateTeamNotifications');
    Route::post('/team/leave', 'TeamInviteController@leaveTeam');
});


