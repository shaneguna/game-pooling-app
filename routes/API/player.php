<?php

Route::group(['namespace' => 'Player'], function () {
    Route::resource('player', 'PlayerController', ['except' => ['create', 'show']]);
    Route::get('/players', 'PlayerController@getList');
    Route::get('/get-favorite-games', 'PlayerController@getPlayerGameFavorites');
    Route::get('/player/{id}/profile', 'PlayerController@getPlayerByID');
    Route::post('/player/{id}/status', 'PlayerController@status');
    Route::post('/players/byPage', 'PlayerController@byPage');
    Route::post('/players/team', 'PlayerController@teamMembers');
    Route::get('/players/{id}/teams-lead-by-player', 'PlayerController@getTeamsLeadByPlayer');

    /*
    * PLayer Chat
    */
     Route::post('/player/messages', 'PlayerChatController@getMessages');
     Route::post('/player/send-message', 'PlayerChatController@sendMessage');
     Route::post('/player/messages-sender', 'PlayerChatController@getPlayerMessageSender');
});


