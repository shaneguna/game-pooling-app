<?php

Route::group(['namespace' => 'User'], function () {
    Route::resource('user', 'AdminUserController', ['except' => ['create', 'show']]);
    Route::get('/users', 'AdminUserController@getList');
	Route::post('/users/{id}/status', 'AdminUserController@status');
});


