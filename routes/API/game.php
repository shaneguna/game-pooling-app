<?php

Route::group(['namespace' => 'Game'], function () {
    Route::resource('game', 'GameController', ['except' => ['create','show']]);
    Route::get('/games', 'GameController@getList');
    Route::get('/games/{id}/category', 'GameController@getGamesByCategory');
    Route::get('/games/{id}/search', 'GameController@getSearchedResult');
    Route::get('/game-details-by-id/{id}', 'GameController@getGameDetailsById');
    Route::post('/game/{id}/status', 'GameController@status');
    Route::post('/games/byPage', 'GameController@byPage');
});


