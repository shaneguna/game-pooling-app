<?php

Route::group(['namespace' => 'Match'], function () {
    Route::resource('match', 'MatchController', ['except' => ['create', 'show']]);
    Route::post('match-create', 'MatchController@store');
    Route::get('/match-possible-participants', 'MatchController@getPossibleMatchPlayers');
    Route::get('/matches', 'MatchController@getList');
    Route::get('/match/{id}', 'MatchController@getByMatchId');
    Route::get('/match-participants', 'MatchController@getMatchParticipantsByMatchId');
    Route::get('/match-rules/{id}', 'MatchController@getMatchRulesByMatchId');
    Route::get('/matches/{id}', 'MatchController@getMatchesByPlayerID');
    Route::post('/match/{id}/status', 'MatchController@status');
    Route::get('/matches-by-player-game','MatchController@getMatchesByPlayerAndGame');
    Route::get('/matches-by-game/{id}', 'MatchController@getListByGameId');
    Route::get('/matches-by-player/{id}','MatchController@getMatchesByPlayerID');
    Route::post('/match-join','MatchController@joinMatch');
    Route::get('/matches-by-vicinity','MatchController@getMatchesByVicinity');

    Route::post('/matches/byPage', 'MatchController@byPage');

    Route::post('/match/message', 'MatchChatController@sendMessage');
    Route::post('/match/chat', 'MatchChatController@matchMessages');
    Route::post('/matches/{match_id}/participants', 'MatchController@getPlayersInMatches');
    Route::post('/match/{match_id}/is-chat-enabled', 'MatchChatController@isChatEnabled');
    
    Route::post('/match/request-to-join', 'MatchController@requestToJoinMatch');
    Route::post('/match/details', 'MatchController@getMatchDetails');
    Route::put('/match/invite-status/update', 'MatchController@updateMatchInviteStatus');
    Route::put('/match/confirmation/update', 'MatchController@updateMatchConfirmation');


    Route::get('/matches/player/request-or-invite', 'MatchController@matchesPlayerInvitesOrRequestsByPlayerID');
    Route::get('/matches/team/request-or-invite', 'MatchController@matchesTeamInvitesOrRequestsByPlayerID');

});


