<?php

Route::group(['namespace' => 'Announcement'], function () {
    Route::resource('announcement', 'AnnouncementController', ['except' => ['create','show']]);
    Route::get('/announcement', 'AnnouncementController@getList');
    Route::get('/announcement-by-id/{name}', 'AnnouncementController@getAnnouncementsById');
});


