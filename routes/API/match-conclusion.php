<?php
Route::group(['namespace' => 'MatchConclusion'], function () {
    Route::resource('match-conclusion', 'MatchConclusionController')->only([
        'index', 'store','update'
    ]);
});
