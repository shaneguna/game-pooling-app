<?php
Route::group(['namespace' => 'SocialNetwork'], function () {
    Route::post('send-friend-request', 'SocialNetworkController@sendFriendRequest');
    Route::post('accept-friend-requests', 'SocialNetworkController@acceptPendingFriendRequest');

    Route::get('get-friend-list/{player_id}', 'SocialNetworkController@getFriendList');
    Route::get('get-friend-requests/{player_id}', 'SocialNetworkController@getUserPendingRequests');

    Route::get('get-favorite-games', 'SocialNetworkController@getFavoriteGames');
    Route::post('add-favorite-game', 'SocialNetworkController@addFavoriteGame');
    Route::post('remove-favorite-game', 'SocialNetworkController@removeFavoriteGame');
});