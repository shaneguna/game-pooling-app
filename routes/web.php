<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::prefix('admin')->group(function () {
    Route::get('{vueRoutes}', function () {
        return view('admin');
    })->where('vueRoutes', '^((?!api).)*$');
});

Route::prefix('/')->group(function () {
    Route::get('{vueRoutes}', function () {
        return view('mainpage');
    })->where('vueRoutes', '^((?!api).)*$')
        ->where('vueRoutes', '^((?!storage).)*$');
});

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');


